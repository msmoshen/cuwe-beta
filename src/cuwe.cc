#include "common.h"
#include "tagger.h"
#include "cmdline.h"

bool MODE_TRAIN = false;
bool WEIGHT_AVERAGED = false;
bool MODE_CONFIG = false;
bool MODE_NBEST = false;
bool MODE_CACHE = false;
bool MODE_STRUCT = false;
bool MODE_JUMAN = false;
bool MODE_FILTER = false;
bool MODE_MAXSUB = false;
bool MODE_CLEAR = false;
bool MODE_DATTR = false;

std::map<std::string, double> feature_weight;
std::map<std::string, double> feature_weight_sum;
std::map<std::string, int> feature_count;
std::map<int, std::map<int, std::vector<int> > > boundary_estimation_table;
int num_of_sentences_processed = 0;

void option_proc(cmdline::parser &option, int argc, char **argv) {

	option.add<std::string>("dics", 'd', "dic header", false, "dic");

	option.add<std::string>("feature", 'f', "feature template filename", false,
			"data/feature.def");

	option.add<unsigned int>("switch", 'K', "generic switching option", false,
			0);

	option.add<std::string>("subfeature", 'F',
			"max-sub feature template filename", false,
			"data/feature.maxsub.def");

	option.add<unsigned int>("window", 'W',
			"initial window size of frequent substring retrieving", false, 3);

	option.add<unsigned int>("config", 'C', "config mode", false, 0);

	option.add<std::string>("file", 'J', "file name of input data", false,
			"data/data.cof");

	option.add<std::string>("data", 'D', "read-in generic data", false,
			"data/data.cof");

	option.add<std::string>("struct", 'S', "read-in structured data", false,
			"data/data.cof");

	option.add<std::string>("punctuation", 'P', "punctuation table", false,
			"data/punc.tab");

	option.add<unsigned int>("division", 'V', "data/division", false, 1);

	option.add<unsigned int>("N-best", 'N', "N-best output", false, 1);

	option.add<int>("cache", 'H', "cache mode", false, 5);

	option.add<int>("left-filter", 'L', "filtering level from left-hand side",
			false, 0);
	option.add<int>("right-filter", 'R', "filtering level from right-hand side",
			false, 0);

	option.add<unsigned int>("maxsub", 'U', "utilize maximized substrs", false,
			0);

	option.add<unsigned int>("freeze", 'z', "freeze state trigger", false, 0);
	option.add<int>("decay-gap", 'g', "gap between per decay", false, 100);
	option.add<int>("reflush-period", 'r', "refulsh hash periodically", false,
			INT_MAX);

	option.add<unsigned int>("threshold", 'o', "freq threshold", false, 1);

	option.add<unsigned int>("bytes", 'Y', "unified byte number", false, 3);
	option.add<unsigned int>("filter", 'T',
			"filtering unreliable boundary estimation", false, 2);

	option.add<int>("printing", 'G', "printing option", false, -1);

	option.add<int>("verbose", 'B', "verbose mode", false, 0); // -1 for silent mode

	option.add<int>("clear", 'Q', "clear structure", false, 0);

	option.add("dattrs", 'M', "include dic attributes in structured output");

	option.add("debug", '\0', "debug mode");
	option.add("version", 'v', "print version");
	option.add("help", 'h', "print this message");

	option.parse_check(argc, argv);
	if (option.exist("version")) {
		cout << VERSION << endl;
		exit(0);
	}
	if (option.exist("train")) {
		MODE_TRAIN = true;
	}
	if (option.exist("averaged")) {
		WEIGHT_AVERAGED = true;
	}

	if (option.exist("config")) {
		MODE_CONFIG = true;
	}

	if (option.exist("N-best")) {
		MODE_NBEST = true;
	}

	if (option.exist("cache")) {
		MODE_CACHE = true;
	}

	if (option.exist("struct")) {
		MODE_STRUCT = true;
	}

	if (option.exist("filter")) {
		MODE_FILTER = true;
	}

	if (option.exist("maxsub")) {
		MODE_MAXSUB = true;
	}

	if (option.exist("clear")) {
		MODE_CLEAR = true;
	}

	if (option.exist("dattrs")) {
		MODE_DATTR = true;
	}

}

int main(int argc, char** argv) {
	cmdline::parser option;
	option_proc(option, argc, argv);

	Morph::Parameter param(option.get<std::string>("dics"),
			option.get<std::string>("feature"),
			option.get<std::string>("subfeature"),
			option.get<unsigned int>("iteration"), true,
			option.exist("shuffle"), option.get<unsigned int>("unk_max_length"),
			option.exist("debug"), option.get<unsigned int>("division"),
			option.get<unsigned int>("N-best"),
			option.get<unsigned int>("freeze"),
			option.get<unsigned int>("window"),
			option.get<unsigned int>("bytes"),
			option.get<unsigned int>("config"),
			option.get<unsigned int>("filter"), option.get<int>("printing"),
			option.get<unsigned int>("switch"),
			option.get<unsigned int>("threshold"), option.get<int>("verbose"),
			option.get<int>("cache"), option.get<int>("decay-gap"),
			option.get<int>("reflush-period"), option.get<int>("left-filter"),
			option.get<int>("right-filter"));

	Morph::Tagger tagger(&param);

	if (option.get<unsigned int>("config") == 1) {
		tagger.count_freq_segment(option.get<std::string>("file"),
				option.get<std::string>("data"),
				option.get<std::string>("struct"),
				option.get<std::string>("punctuation"));
	} else if (option.get<unsigned int>("config") == 2) {
		tagger.read_freq_struct_data(option.get<std::string>("data"),
				option.get<std::string>("punctuation"));
	} else if (option.get<unsigned int>("config") == 3) {
		tagger.check_bytes_unification(option.get<std::string>("data"),
				option.get<std::string>("punctuation"));
	} else {
		cerr << ";; not implemented" << endl;
	}

	feature_weight.clear();
	return 0;
}
