#ifndef SENTENCE_H
#define SENTENCE_H

#include "common.h"
#include "feature.h"
#include "dic.h"
#include "parameter.h"
#include "hashFreq.h"

namespace Morph {

//#define UNIFYING_FACTOR 10

class Sentence {
	Parameter *param;
	Dic *dic;
	FeatureTemplateSet *ftmpl;
	FeatureTemplateSet *subftmpl;
	std::vector<Node *> *begin_node_list; // position -> list of nodes that begin at this pos
	std::vector<Node *> *end_node_list; // position -> list of nodes that end at this pos

	bool unknown_word_detection;
	size_t word_num;
	const char *sentence_c_str;
	FeatureSet *feature;
	FeatureSet *subfeature;
	unsigned int reached_pos;
	unsigned int reached_pos_of_pseudo_nodes;

public:

	unsigned int length; // length of this sentence
	std::string sentence;

	std::map<int, int> bound_index;

	Node* first_node;
	std::map<int, std::vector<Morph::BoundAttrs *> > beginBoundAttrsTab;
	std::map<int, std::vector<Morph::BoundAttrs *> > endBoundAttrsTab;

	const std::map<int, std::vector<Morph::BoundAttrs *> >* refBeginBoundAttrsTab;
	const std::map<int, std::vector<Morph::BoundAttrs *> >* refEndBoundAttrsTab;

	Sentence(std::vector<Node *> *in_begin_node_list,
			std::vector<Node *> *in_end_node_list, std::string &in_sentence,
			Dic *in_dic, FeatureTemplateSet *in_ftmpl,
			FeatureTemplateSet *in_subftmpl, Parameter *in_param,
			const std::map<int, std::vector<Morph::BoundAttrs *> >* in_refBeginBoundAttrsTab,
			const std::map<int, std::vector<Morph::BoundAttrs *> >* in_refEndBoundAttrsTab);
	Sentence(std::vector<Node *> *in_begin_node_list,
			std::vector<Node *> *in_end_node_list, std::string &in_sentence,
			Dic *in_dic, FeatureTemplateSet *in_ftmpl,
			FeatureTemplateSet *in_subftmpl, Parameter *in_param);
	Sentence(size_t max_byte_length, std::vector<Node *> *in_begin_node_list,
			std::vector<Node *> *in_end_node_list, Dic *in_dic,
			FeatureTemplateSet *in_ftmpl, FeatureTemplateSet *in_subftmpl,
			Parameter *in_param);
	void init(size_t max_byte_length, std::vector<Node *> *in_begin_node_list,
			std::vector<Node *> *in_end_node_list, Dic *in_dic,
			FeatureTemplateSet *in_ftmpl, FeatureTemplateSet *in_subftmpl,
			Parameter *in_param);
	~Sentence();
	void clear_nodes();
	bool lookup_and_analyze();
	bool add_one_word(std::string &word);
	std::string &get_sentence() {
		return sentence;
	}
	FeatureSet *get_feature() {
		return feature;
	}
	FeatureSet *set_feature() {
		if (feature)
			delete feature;
		feature = (*begin_node_list)[length]->feature;
		(*begin_node_list)[length]->feature = NULL;
		return feature;
	}

	FeatureSet *get_subfeature() {
//		if (!subfeature)
//			subfeature = NULL;

		return subfeature;
	}

	FeatureSet *set_subfeature() {
		if ((*begin_node_list)[length]->subFeature) {
			subfeature = (*begin_node_list)[length]->subFeature;
		} else {
			subfeature = NULL;
		}

		return subfeature;
	}

	FeatureTemplateSet* get_feature_template() {
		return ftmpl;
	}

	FeatureTemplateSet* get_feature_sub_template() {
		return subftmpl;
	}

	void feature_print();

	void subfeature_print() {
		if (subfeature)
			subfeature->print();
	}

	unsigned int get_length() {
		return length;
	}

	Node* get_bos_node_list(unsigned int pos) {
		return (*begin_node_list)[pos];
	}
	Node* get_eos_node_list(unsigned int pos) {
		return (*end_node_list)[pos];
	}

	Node *get_bos_node();
	Node *get_eos_node();
	Node *find_best_path();
	void set_begin_node_list(unsigned int pos, Node *new_node);
	void set_end_node_list(unsigned int pos, Node *r_node);
	bool viterbi_at_position(unsigned int pos, Node *r_node);
	unsigned int find_reached_pos(unsigned int pos, Node *node);
	unsigned int find_reached_pos_of_pseudo_nodes(unsigned int pos, Node *node);
	void print_best_path();

	void mark_best_path();

	void print_lattice();
	void minus_feature_from_weight(
			std::map<std::string, double> &in_feature_weight);
	void minus_feature_from_weight(
			std::map<std::string, double> &in_feature_weight, size_t factor);

	void minus_subfeature_from_weight(
			std::map<std::string, double> &in_feature_weight, size_t factor);

	void minus_subfeature_from_weight(
			std::map<std::string, double> &in_feature_weight,
			std::map<std::string, int> &in_feature_count, int factor);

	bool lookup_gold_data(std::string &word_pos_pair);
	Node *lookup_and_make_special_pseudo_nodes(const char *start_str,
			unsigned int pos);
	Node *lookup_and_make_special_pseudo_nodes(const char *start_str,
			unsigned int specified_length, std::string *specified_pos);
	Node *lookup_and_make_special_pseudo_nodes(const char *start_str,
			unsigned int pos, unsigned int specified_length,
			std::string *specified_pos);
	Node *make_unk_pseudo_node_list_from_previous_position(
			const char *start_str, unsigned int previous_pos);
	Node *make_unk_pseudo_node_list_from_some_positions(const char *start_str,
			unsigned int pos, unsigned int previous_pos);
	Node *make_unk_pseudo_node_list_by_dic_check(const char *start_str,
			unsigned int pos, Node *r_node, unsigned int specified_char_num);

	bool lookup_and_analyze_nbest();
	bool viterbi_at_position_nbest(unsigned int pos, Node *r_node);
	int assign_id(int base_id, unsigned int pos, Node *r_node);
	Node *find_N_best_path();
	void print_N_best_path();
	void print_Juman_style_path();

	void evaluate_unigram_maxsub_feature() {
		if (refBeginBoundAttrsTab != NULL) {

//			cerr << "a new sentence for ms analysis: "
//					<< refBeginBoundAttrsTab->size() << " ";

			for (std::map<int, std::vector<Morph::BoundAttrs *> >::const_iterator bmap_it =
					refBeginBoundAttrsTab->begin();
					bmap_it != refBeginBoundAttrsTab->end(); ++bmap_it) {

				if (!bmap_it->second.empty()) {

					Node* begin_node = (*begin_node_list)[bmap_it->first];
					while (begin_node) {

//						cerr << "node: " << *begin_node->string_for_print
//								<< endl;

						for (std::vector<Morph::BoundAttrs *>::const_iterator bvec_it =
								bmap_it->second.begin();
								bvec_it != bmap_it->second.end(); ++bvec_it) {

							if (begin_node->length
									> (*bvec_it)->max_substr.length()) {
//								cerr
//										<< ";;odd: begin node length longer than maxsub"
//										<< endl;
							} else {
								FeatureSet *f = new FeatureSet(subftmpl);
								f->extract_maxsub_begin_unigram_feature(
										begin_node, *bvec_it);
								begin_node->wcost_ms +=
										f->calc_inner_product_with_weight();

								if (begin_node->subFeature != NULL) {
									for (std::vector<std::string>::iterator f_it =
											f->fset.begin();
											f_it != f->fset.end(); ++f_it) {
										begin_node->subFeature->fset.push_back(
												*f_it);
									}
								} else {
									begin_node->subFeature = f;
								}

								//							if (begin_node->wcost_ms != 0)
								//								cerr << "wcost_ms: " << begin_node->wcost_ms
								//										<< endl;
							}

						}

						begin_node = begin_node->bnext;
					}

				}
			}

//			cerr << endl;

		} else {
//			cerr << ";;warn: ref bbat null" << endl;
		}

		if (refEndBoundAttrsTab != NULL) {

//			cerr << "a new sentence for ms analysis: "
//					<< refEndBoundAttrsTab->size() << " ";

			for (std::map<int, std::vector<Morph::BoundAttrs *> >::const_iterator emap_it =
					refEndBoundAttrsTab->begin();
					emap_it != refEndBoundAttrsTab->end(); ++emap_it) {

				if (!emap_it->second.empty()) {

					Node* end_node = (*end_node_list)[emap_it->first];
					while (end_node) {

//						cerr << "node: " << *begin_node->string_for_print
//								<< endl;

						for (std::vector<Morph::BoundAttrs *>::const_iterator evec_it =
								emap_it->second.begin();
								evec_it != emap_it->second.end(); ++evec_it) {

							if (end_node->length
									> (*evec_it)->max_substr.length()) {
//								cerr
//										<< ";;odd: end node length longer than maxsub"
//										<< endl;
							} else {
								FeatureSet *f = new FeatureSet(subftmpl);
								f->extract_maxsub_end_unigram_feature(end_node,
										*evec_it);
								end_node->wcost_ms +=
										f->calc_inner_product_with_weight();

								if (end_node->subFeature != NULL) {
									for (std::vector<std::string>::iterator f_it =
											f->fset.begin();
											f_it != f->fset.end(); ++f_it) {
										end_node->subFeature->fset.push_back(
												*f_it);
									}
								} else {
									end_node->subFeature = f;
								}

								//							if (begin_node->wcost_ms != 0)
								//								cerr << "wcost_ms: " << end_node->wcost_ms
								//										<< endl;
							}

						}

						end_node = end_node->enext;
					}

				}
			}

//			cerr << endl;

		} else {
//			cerr << ";;warn: ref bbat null" << endl;
		}

	}

	void evaluate_unigram_maxsub_feature_along_gold_path(unsigned int pos,
			Node *r_node) {
		if (refBeginBoundAttrsTab != NULL) {

			std::map<int, std::vector<Morph::BoundAttrs *> >::const_iterator bmap_finder =
					refBeginBoundAttrsTab->find(pos);

			if (bmap_finder != refBeginBoundAttrsTab->end()) {

				if (!bmap_finder->second.empty()) {

					for (std::vector<Morph::BoundAttrs *>::const_iterator bvec_it =
							bmap_finder->second.begin();
							bvec_it != bmap_finder->second.end(); ++bvec_it) {

						if (r_node->length > (*bvec_it)->max_substr.length()) {
//							cerr
//									<< ";;odd: begin node length longer than maxsub"
//									<< endl;
						} else {
							FeatureSet *f = new FeatureSet(subftmpl);
							f->extract_maxsub_begin_unigram_feature(r_node,
									*bvec_it);
							r_node->wcost_ms +=
									f->calc_inner_product_with_weight();

							if (r_node->subFeature != NULL) {
								for (std::vector<std::string>::iterator f_it =
										f->fset.begin(); f_it != f->fset.end();
										++f_it) {
									r_node->subFeature->fset.push_back(*f_it);
								}
							} else {
								r_node->subFeature = f;
							}

							//							if(r_node->subFeature)
							//								r_node->subFeature->print();

							//							if (r_node->wcost_ms != 0)
							//								cerr << "wcost_ms: " << r_node->wcost_ms
							//										<< endl;

//							cerr << "begin:  " << *r_node->string_for_print
//									<< " " << (*bvec_it)->max_substr << endl;
						}

					}

				}

			}

		} else {
//			cerr << ";;warn: ref bbat null" << endl;
		}

		if (refEndBoundAttrsTab != NULL) {

//			cerr << "a new sentence for ms analysis: "
//					<< refEndBoundAttrsTab->size() << " ";

			std::map<int, std::vector<Morph::BoundAttrs *> >::const_iterator emap_finder =
					refEndBoundAttrsTab->find(pos + r_node->length);

			if (emap_finder != refEndBoundAttrsTab->end()) {

				if (!emap_finder->second.empty()) {

					for (std::vector<Morph::BoundAttrs *>::const_iterator evec_it =
							emap_finder->second.begin();
							evec_it != emap_finder->second.end(); ++evec_it) {

						if (r_node->length > (*evec_it)->max_substr.length()) {
//							cerr << ";;odd: end node length longer than maxsub"
//									<< endl;
						} else {
							FeatureSet *f = new FeatureSet(subftmpl);
							f->extract_maxsub_end_unigram_feature(r_node,
									*evec_it);
							r_node->wcost_ms +=
									f->calc_inner_product_with_weight();

							if (r_node->subFeature != NULL) {
								for (std::vector<std::string>::iterator f_it =
										f->fset.begin(); f_it != f->fset.end();
										++f_it) {
									r_node->subFeature->fset.push_back(*f_it);
								}
							} else {
								r_node->subFeature = f;
							}

//							cerr << "end:  " << *r_node->string_for_print << " "
//									<< (*evec_it)->max_substr << endl;
						}

					}

				}

			}

		} else {
//			cerr << ";;warn: ref bbat null" << endl;
		}
	}

	void evaluate_unigram_maxsub_path() {

		Node *node = (*begin_node_list)[length]->prev;
		if (node->next)
			cerr << ";;err: forward link already exists." << endl;
		node->next = (*begin_node_list)[length];
		int pos = length - node->length;

		while (node) {

//			cerr << "current pos|node_length:" << pos << " " << node->length
//					<< " " << *(node->string_for_print) << endl;

			evaluate_unigram_maxsub_feature_along_gold_path(pos, node);

			if (node->prev) {
				Node *tmp_node = node;
				node = node->prev;
				if (node->next)
					cerr << ";;err: forward link already exists." << endl;
				node->next = tmp_node;
				if (pos > 0) {
					pos -= node->length;
				} else {
					first_node = node->next;
					break;
				}
			} else {
				break;
			}

		}

		if (pos != 0)
			cerr << ";; cannot analyze:" << sentence << endl;
	}

	void set_gold_subfeature(Node *r_node) {
		if (r_node->prev) {
			Node *l_node = r_node->prev;
			if (l_node->subFeature) {
				if (r_node->subFeature) {
					r_node->subFeature->append_feature(l_node->subFeature);
				} else {
					r_node->subFeature = l_node->subFeature;
				}
			}
		}
	}

	void plus_subfeature_from_weight(
			std::map<std::string, double> &in_feature_weight, int factor) {
		Node *node = (*begin_node_list)[length]; // EOS
		if (node->subFeature)
			node->subFeature->plus_feature_from_weight(in_feature_weight,
					factor);
	}

};

}

#endif
