#include "common.h"
#include "tagger.h"
#include "sentence.h"

namespace Morph {

Tagger::Tagger(Parameter *in_param) {
	param = in_param;

	ftmpl.open(param->ftmpl_filename);
	subftmpl.open(param->subftmpl_filename);

	dic.open(param, &ftmpl);
	rev_dic.rev_open(param, &ftmpl);

	begin_node_list.reserve(INITIAL_MAX_INPUT_LENGTH);
	end_node_list.reserve(INITIAL_MAX_INPUT_LENGTH);

	sentences_for_train_num = 0;

	bytes_unification_checked = false;
}

Tagger::~Tagger() {
}

// analyze a sentence
Sentence *Tagger::new_sentence_analyze(std::string &in_sentence) {
	sentence = new Sentence(&begin_node_list, &end_node_list, in_sentence, &dic,
			&ftmpl, &subftmpl, param);
	if (MODE_NBEST) {
		sentence->lookup_and_analyze_nbest();
	} else {
		sentence->lookup_and_analyze();
	}

	return sentence;
}

Sentence *Tagger::new_sentence_analyze_transparent(std::string& in_sentence,
		const std::map<int, std::vector<Morph::BoundAttrs *> >* in_refBeginBoundAttrsTab,
		const std::map<int, std::vector<Morph::BoundAttrs *> >* in_refEndBoundAttrsTab) {
	sentence = new Sentence(&begin_node_list, &end_node_list, in_sentence, &dic,
			&ftmpl, &subftmpl, param, in_refBeginBoundAttrsTab,
			in_refEndBoundAttrsTab);
	if (MODE_NBEST) {
		sentence->lookup_and_analyze_nbest();
	} else {
		sentence->lookup_and_analyze();
	}

	return sentence;
}

Sentence *Tagger::new_sentence_analyze_transparent(std::string &in_sentence,
		int current_sentence,
		const std::map<int, std::map<int, std::vector<Morph::BoundAttrs *> > >& in_refAryOfBeginBoundAttrsTab,
		const std::map<int, std::map<int, std::vector<Morph::BoundAttrs *> > >& in_refAryOfEndBoundAttrsTab) {

	std::map<int, std::map<int, std::vector<Morph::BoundAttrs *> > >::const_iterator b_ary_finder =
			in_refAryOfBeginBoundAttrsTab.find(current_sentence);
	std::map<int, std::map<int, std::vector<Morph::BoundAttrs *> > >::const_iterator e_ary_finder =
			in_refAryOfEndBoundAttrsTab.find(current_sentence);

	const std::map<int, std::vector<Morph::BoundAttrs *> >* in_refBeginBoundAttrsTab;
	const std::map<int, std::vector<Morph::BoundAttrs *> >* in_refEndBoundAttrsTab;

	if (b_ary_finder != in_refAryOfBeginBoundAttrsTab.end()) {
		in_refBeginBoundAttrsTab = &(b_ary_finder->second);
	} else {
		in_refBeginBoundAttrsTab = NULL;
	}
	if (e_ary_finder != in_refAryOfEndBoundAttrsTab.end()) {
		in_refEndBoundAttrsTab = &(e_ary_finder->second);
	} else {
		in_refEndBoundAttrsTab = NULL;
	}

	sentence = new Sentence(&begin_node_list, &end_node_list, in_sentence, &dic,
			&ftmpl, &subftmpl, param, in_refBeginBoundAttrsTab,
			in_refEndBoundAttrsTab);
	if (MODE_NBEST) {
		sentence->lookup_and_analyze_nbest();
	} else {
		sentence->lookup_and_analyze();
	}

	return sentence;
}

// clear a sentence
void Tagger::sentence_clear() {
	delete sentence;
}

// train
bool Tagger::train() {

//         for iteration 1..5 {
//                 for sentence 1..N {
//                          tagger.analyzer(sentence chunked);
//                          - collected feature;
//                          feature gold standard;
//                          +
//                     }
//             }

	int total_iteration_num = 0;
	int processed_num = 0;

	for (size_t t = 0; t < param->iteration_num; t++) {
		cerr << "ITERATION:" << t << endl;
		if (param->shuffle_training_data) // shuffle training data
			random_shuffle(sentences_for_train.begin(),
					sentences_for_train.end());
		for (std::vector<Sentence *>::iterator it = sentences_for_train.begin();
				it != sentences_for_train.end(); it++) {

			if (total_iteration_num % param->division == 0) {

				processed_num++;
				cerr << "sentence: " << processed_num << endl;
				cerr << (*it)->get_sentence() << endl;

				if (MODE_MAXSUB) {
					new_sentence_analyze_transparent((*it)->get_sentence(),
							&(*it)->beginBoundAttrsTab,
							&(*it)->endBoundAttrsTab);

					sentence->minus_subfeature_from_weight(feature_weight,
							feature_count, 1); // - ms prediction
					if ((*it)->get_subfeature()) {
						(*it)->get_subfeature()->plus_feature_from_weight(
								feature_weight, feature_count, 1);
					}

					if (WEIGHT_AVERAGED) { // for average
						sentence->minus_subfeature_from_weight(
								feature_weight_sum, total_iteration_num);
						if ((*it)->get_subfeature()) {
							(*it)->get_subfeature()->plus_feature_from_weight(
									feature_weight_sum, total_iteration_num);
						}
					}

				} else {
					new_sentence_analyze((*it)->get_sentence()); // get the best path
				}

				sentence->minus_feature_from_weight(feature_weight); // - prediction
				(*it)->get_feature()->plus_feature_from_weight(feature_weight); // + gold standard
				if (WEIGHT_AVERAGED) { // for average
					sentence->minus_feature_from_weight(feature_weight_sum,
							total_iteration_num); // - prediction
					(*it)->get_feature()->plus_feature_from_weight(
							feature_weight_sum, total_iteration_num); // + gold standard
				}
				sentence_clear();
			}

			total_iteration_num++;
		}
	}

	cerr << "Total sentence: " << total_iteration_num << endl;

	if (WEIGHT_AVERAGED) {
		for (std::map<std::string, double>::iterator it =
				feature_weight_sum.begin(); it != feature_weight_sum.end();
				it++) {
			feature_weight[it->first] -= it->second / total_iteration_num;
		}
	}

//     for (std::vector<Sentence *>::iterator it = sentences_for_train.begin(); it != sentences_for_train.end(); it++) {
//         new_sentence_analyze((*it)->get_sentence()); // get the best path
//         print_best_path();
//         sentence_clear();
//     }

	clear_gold_data();
	return true;
}

// read gold standard data
bool Tagger::read_gold_data(const std::string &gsd_file) {
	return read_gold_data(gsd_file.c_str());
}

// read gold standard data
bool Tagger::read_gold_data(const char *gsd_file) {
	std::ifstream gsd_in(gsd_file, std::ios::in);
	if (!gsd_in.is_open()) {
		cerr << ";; cannot open gold data for reading" << endl;
		return false;
	}

	std::string buffer;
	while (getline(gsd_in, buffer)) {
		if (buffer.at(0) == '#') { // comment line (article delimiter)
			continue;
		}
		std::vector<std::string> word_pos_pairs;
		split_string(buffer, " ", word_pos_pairs);

		Sentence *new_sentence = new Sentence(strlen(buffer.c_str()),
				&begin_node_list, &end_node_list, &dic, &ftmpl, &subftmpl,
				param);
		for (std::vector<std::string>::iterator it = word_pos_pairs.begin();
				it != word_pos_pairs.end(); it++) {
			new_sentence->lookup_gold_data(*it);
		}
		new_sentence->find_best_path();
		new_sentence->set_feature();

		new_sentence->clear_nodes();

		add_one_sentence_for_train(new_sentence);
		// new_sentence->feature_print();
		sentences_for_train_num++;
	}

	gsd_in.close();
	return true;
}

bool Tagger::read_gsd_and_load_bound_attrs(const std::string &gsd_file,
		const std::string &main_struct_file,
		const std::string &side_struct_file, const std::string &punc_file) {
	return read_gsd_and_load_bound_attrs(gsd_file.c_str(),
			main_struct_file.c_str(), side_struct_file.c_str(),
			punc_file.c_str());
}

//bool Tagger::read_gsd_and_load_bound_attrs(const char *gsd_file, const char *main_struct_file,
//		const char *side_struct_file, const char *punc_file) {
//
//	read_freq_struct_data(side_struct_file, punc_file);
//
//	cascade_freq_struct_data(main_struct_file, punc_file);
//
//	HashFreqReadOnly.open_deck(sentences_for_hash);
//
//	read_gold_data(gsd_file);
//
//	int charOffset = param->freq_substr_window;
//
//	int sentences_counter = 0;
//
//	for (std::vector<Sentence *>::iterator it = sentences_for_train.begin();
//			it != sentences_for_train.end(); it++) {
//
//		if((*it)->sentence.length() != (*it)->length)	{
//			cerr<<";;err: length of sentence not consistent"<<endl;
//		}
//
//		if ((*it)->sentence.length()
//						<= charOffset * utf8_bytes((unsigned char *) (*it)->sentence.c_str())) {
//			sentences_for_hash.push_back((*it)->sentence);
//			++sentences_counter;
//			continue;
//		}
//
//		//		std::vector<std::string> word_pos_pairs;
//		//		split_string(buffer, " ", word_pos_pairs);
//
//				sentences_for_hash.push_back((*it)->sentence);
//
//				int lookup_stat = 0;
//				int begin_pos = 0;
//
//				int i = begin_pos * utf8_bytes((unsigned char *) (*it)->sentence.c_str());
//
//				while (i
//						<= (*it)->sentence.length()
//								- charOffset
//										* utf8_bytes((unsigned char *) (*it)->sentence.c_str())) {
//
//					std::string newEntry = (*it)->sentence.substr(i,
//							charOffset * utf8_bytes((unsigned char *) (*it)->sentence.c_str()));
//
//					if (param->verbose_option == 1)
//						cerr << "lookup entry: " << newEntry << endl;
//
//					//pre-checking block (unfinished)
//		//			lookup_stat = HashFreqReadOnly.lookup(newEntry, sentences_counter,
//		//					i, boundary_estimation_table, param->filter_size);
//		//
//		//			if (lookup_stat < 0) {
//		//				cerr << "terminated unexpectedly in looking up." << endl;
//		//				break;
//		//			}
//
//					Morph::BoundAttrs* boundAttrsRef =
//							HashFreqReadOnly.lookup_and_load_bound_attrs(newEntry,
//									sentences_counter, i, param->filter_size);
//
//					if (boundAttrsRef != NULL) {
//						if ((*it)->beginBoundAttrsTab.find(
//								i)
//								== (*it)->beginBoundAttrsTab.end()) {
//							std::vector<Morph::BoundAttrs *> newBAC;
//							newBAC.push_back(boundAttrsRef);
//							(*it)->beginBoundAttrsTab[i] =
//									newBAC;
//
//							if (param->verbose_option == 1)
//								cerr << "new position for beginBoundAttrs: " << i << endl;
//
//						} else {
//							(*it)->beginBoundAttrsTab[i].push_back(
//									boundAttrsRef);
//
//							if (param->verbose_option == 1)
//								cerr << "more on position for beginBoundAttrs: " << i << endl;
//						}
//
//						if ((*it)->endBoundAttrsTab.find(
//								boundAttrsRef->end_pos)
//								== (*it)->endBoundAttrsTab.end()) {
//							std::vector<Morph::BoundAttrs *> newBAC;
//							newBAC.push_back(boundAttrsRef);
//							(*it)->endBoundAttrsTab[boundAttrsRef->end_pos] =
//									newBAC;
//
//							if (param->verbose_option == 1)
//								cerr << "new position for endBoundAttrs: " << boundAttrsRef->end_pos << endl;
//
//						} else {
//							(*it)->endBoundAttrsTab[boundAttrsRef->end_pos].push_back(
//									boundAttrsRef);
//
//							if (param->verbose_option == 1)
//								cerr << "more on position for endBoundAttrs: " << boundAttrsRef->end_pos << endl;
//						}
//					}
//
//					i += utf8_bytes((unsigned char *) ((*it)->sentence.c_str() + i));
//				}
//
//				cerr<< "current sentence to evaluate: "<< (*it)->sentence <<endl;
//				(*it)->evaluate_unigram_maxsub_path();
//
//				++sentences_counter;
//	}
//
//	if (sentences_counter != sentences_for_train.size()
//			|| sentences_for_hash.size() != sentences_for_train.size()) {
//		cerr << ";;err: sentences not aligned: " << sentences_counter << " "
//				<< sentences_for_hash.size() << " "
//				<< sentences_for_train.size() << endl;
//	}
//
//	cerr << "Sentence count: " << sentences_counter << endl;
//
//	for (std::vector<Sentence *>::iterator it = sentences_for_train.begin();
//			it != sentences_for_train.end(); it++){
//
//		cerr << "pre-test for ms analysis: "<< (*it)->beginBoundAttrsTab.size() << " ";
//
//		for (std::map<int, std::vector<Morph::BoundAttrs *> >::iterator bmap_it =
//				(*it)->beginBoundAttrsTab.begin(); bmap_it != (*it)->beginBoundAttrsTab.end();
//				++bmap_it) {
//
//			if (!bmap_it->second.empty()) {
//
//				cerr << "gotit ";
//
//			}
//		}
//
//		cerr << endl;
//
//	}
//
//	//checking block
//	if (param->printing_option == 0)
//		HashFreqReadOnly.print_structure(param->freq_threshold);
//	if (param->printing_option == 1)
//		HashFreqReadOnly.print_from_lookup();
//	if (param->printing_option == 2)
//		HashFreqReadOnly.print_freq_sorted(param->freq_threshold);
//
//	cerr << "HashFreqRO capacity: " << HashFreqReadOnly.cacheFreq.size()
//			<< endl;
//
//	return true;
//
//}

bool Tagger::read_gsd_and_load_bound_attrs(const char *gsd_file,
		const char *main_struct_file, const char *side_struct_file,
		const char *punc_file) {

	if (param->generic_switch == 0) {

		read_freq_struct_data(side_struct_file, punc_file);
		cascade_freq_struct_data(main_struct_file, punc_file);

	} else if (param->generic_switch == 1) {

		int starting_num = read_freq_data(side_struct_file);
		cascade_freq_data(main_struct_file, starting_num);

	} else {
		cerr << ";;err: switching option not implemented" << endl;
		return false;
	}

	HashFreq.categorize_freq(param->freq_threshold);

	HashFreq.open_deck(sentences_for_hash);

	int charOffset = param->freq_substr_window;

	std::ifstream gsd_in(gsd_file, std::ios::in);
	if (!gsd_in.is_open()) {
		cerr << ";; cannot open gold data for reading" << endl;
		return false;
	}

	std::string buffer;
	while (getline(gsd_in, buffer)) {
		if (buffer.at(0) == '#') { // comment line (article delimiter)
			continue;
		}
		std::vector<std::string> word_pos_pairs;
		split_string(buffer, " ", word_pos_pairs);

		Sentence *new_sentence = new Sentence(strlen(buffer.c_str()),
				&begin_node_list, &end_node_list, &dic, &ftmpl, &subftmpl,
				param);
		for (std::vector<std::string>::iterator it = word_pos_pairs.begin();
				it != word_pos_pairs.end(); it++) {
			new_sentence->lookup_gold_data(*it);
		}
		new_sentence->find_best_path();
		new_sentence->set_feature();

		if (new_sentence->sentence.length() != new_sentence->length) {
			cerr << ";;err: length of sentence not consistent" << endl;
		}

		if (new_sentence->sentence.length()
				<= charOffset
						* utf8_bytes(
								(unsigned char *) new_sentence->sentence.c_str())) {

			new_sentence->set_subfeature();
			new_sentence->clear_nodes();
			add_one_sentence_for_train(new_sentence);
			// new_sentence->feature_print();
			sentences_for_train_num++;

			sentences_for_hash.push_back(new_sentence->sentence);
			continue;
		}

		cerr << "new sentence: " << new_sentence->sentence << endl;

		sentences_for_hash.push_back(new_sentence->sentence);

		if (!bytes_unification_checked
				|| (bytes_unification_checked
						&& activated_sentences.at(sentences_for_train_num))) {

			int begin_pos = 0;
			int i = begin_pos
					* utf8_bytes(
							(unsigned char *) new_sentence->sentence.c_str());

			while (i
					<= new_sentence->sentence.length()
							- charOffset
									* utf8_bytes(
											(unsigned char *) new_sentence->sentence.c_str())) {

				std::string newEntry =
						new_sentence->sentence.substr(i,
								charOffset
										* utf8_bytes(
												(unsigned char *) new_sentence->sentence.c_str()));

				if (param->verbose_option == 1)
					cerr << "lookup entry: " << newEntry << endl;

				//pre-checking block (unfinished)
				//			lookup_stat = HashFreq.lookup(newEntry, sentences_counter,
				//					i, boundary_estimation_table, param->filter_size);
				//
				//			if (lookup_stat < 0) {
				//				cerr << "terminated unexpectedly in looking up." << endl;
				//				break;
				//			}

				std::vector<Morph::BoundAttrs*> boundAttrsArray =
						HashFreq.lookup_and_load_bound_attrs(newEntry,
								new_sentence->sentence, i, param->filter_size);

				if (!boundAttrsArray.empty()) {

					for (std::vector<Morph::BoundAttrs*>::iterator bound_vec_it =
							boundAttrsArray.begin();
							bound_vec_it != boundAttrsArray.end();
							++bound_vec_it) {

						//read freq tags
						if (HashFreq.freq_category_list.find(
								(*bound_vec_it)->freqEntityRef->freq)
								== HashFreq.freq_category_list.end()) {
							cerr << ";;err: freq category not found" << endl;
						} else {
							(*bound_vec_it)->freq_tag =
									HashFreq.freq_category_list[(*bound_vec_it)->freqEntityRef->freq];

//							cerr << (*bound_vec_it)->freqEntityRef->freq << " "
//									<< (*bound_vec_it)->freq_tag << endl;
						}

						if (new_sentence->beginBoundAttrsTab.find(i)
								== new_sentence->beginBoundAttrsTab.end()) {
							std::vector<Morph::BoundAttrs *> newBAC;
							newBAC.push_back(*bound_vec_it);
							new_sentence->beginBoundAttrsTab[i] = newBAC;

							if (param->verbose_option == 1)
								cerr << "new position for beginBoundAttrs: "
										<< i << endl;

						} else {
							new_sentence->beginBoundAttrsTab[i].push_back(
									*bound_vec_it);

							if (param->verbose_option == 1)
								cerr << "more on position for beginBoundAttrs: "
										<< i << endl;
						}

						if (new_sentence->endBoundAttrsTab.find(
								(*bound_vec_it)->end_pos)
								== new_sentence->endBoundAttrsTab.end()) {
							std::vector<Morph::BoundAttrs *> newBAC;
							newBAC.push_back(*bound_vec_it);
							new_sentence->endBoundAttrsTab[(*bound_vec_it)->end_pos] =
									newBAC;

							if (param->verbose_option == 1)
								cerr << "new position for endBoundAttrs: "
										<< (*bound_vec_it)->end_pos << endl;

						} else {
							new_sentence->endBoundAttrsTab[(*bound_vec_it)->end_pos].push_back(
									*bound_vec_it);

							if (param->verbose_option == 1)
								cerr << "more on position for endBoundAttrs: "
										<< (*bound_vec_it)->end_pos << endl;
						}

					}

				}

				i += utf8_bytes(
						(unsigned char *) (new_sentence->sentence.c_str() + i));
			}

		}

		new_sentence->refBeginBoundAttrsTab =
				&(new_sentence->beginBoundAttrsTab);
		new_sentence->refEndBoundAttrsTab = &(new_sentence->endBoundAttrsTab);

		new_sentence->evaluate_unigram_maxsub_path();

		Node* node_iter = new_sentence->first_node;

		while (1) {

//			cerr << "now "<< *node_iter->string_for_print << endl;

			new_sentence->set_gold_subfeature(node_iter);

			if (node_iter
					== new_sentence->get_bos_node_list(new_sentence->length))
				break;
			else
				node_iter = node_iter->next;
		}

//		cerr << "finished sentence: " << new_sentence->sentence << endl;

		new_sentence->set_subfeature();
		new_sentence->clear_nodes();

		//debug phrase
//		cerr << "pre-test for ms analysis: "
//				<< new_sentence->beginBoundAttrsTab.size() << " ";
//		for (std::map<int, std::vector<Morph::BoundAttrs *> >::iterator bmap_it =
//				new_sentence->beginBoundAttrsTab.begin();
//				bmap_it != new_sentence->beginBoundAttrsTab.end(); ++bmap_it) {
//			if (!bmap_it->second.empty()) {
//				cerr << "gotit ";
//			}
//		}
//		cerr << endl;
//		new_sentence->subfeature_print();

		add_one_sentence_for_train(new_sentence);
		sentences_for_train_num++;
	}

	//checking block
//	if (param->printing_option == 0)
//		HashFreq.print_structure(1);
	if (param->printing_option == 1)
		HashFreq.print_from_lookup();
	if (param->printing_option == 2)
		HashFreq.print_freq_sorted(param->freq_threshold);

	cerr << "HashFreq capacity: " << HashFreq.cacheFreq.size() << endl;

	gsd_in.close();
	return true;
}

bool Tagger::read_raw_and_load_bound_attrs(const std::string &raw_file,
		const std::string &main_struct_file,
		const std::string &side_struct_file, const std::string &punc_file) {
	return read_raw_and_load_bound_attrs(raw_file.c_str(),
			main_struct_file.c_str(), side_struct_file.c_str(),
			punc_file.c_str());
}

bool Tagger::read_raw_and_load_bound_attrs(const char *raw_file,
		const char *main_struct_file, const char *side_struct_file,
		const char *punc_file) {

	if (param->generic_switch == 0) {

		read_freq_struct_data(side_struct_file, punc_file);
		cascade_freq_struct_data(main_struct_file, punc_file);

	} else if (param->generic_switch == 1) {

		int starting_num = read_freq_data(side_struct_file);
		cascade_freq_data(main_struct_file, starting_num);

	} else {
		cerr << ";;err: switching option not implemented" << endl;
		return false;
	}

	int number_of_sentences_preprocessed = 0;

	HashFreq.categorize_freq(param->freq_threshold);

	HashFreq.open_deck(sentences_for_hash);

	int charOffset = param->freq_substr_window;

	std::ifstream raw_in(raw_file, std::ios::in);
	if (!raw_in.is_open()) {
		cerr << ";; cannot open gold data for reading" << endl;
		return false;
	}

	std::string buffer;
	while (getline(raw_in, buffer)) {

		if (buffer.at(0) == '#') { //comment line (article delimiter)
			continue;
		}

		if (buffer.length()
				<= charOffset * utf8_bytes((unsigned char *) buffer.c_str())) { //line too short
			++number_of_sentences_preprocessed;
			continue;
		}

		Sentence *new_sentence = new Sentence(&begin_node_list, &end_node_list,
				buffer, &dic, &ftmpl, &subftmpl, param);

		cerr << "new sentence: " << buffer << endl;

//		sentences_for_hash.push_back(buffer);

		if (!bytes_unification_checked
				|| (bytes_unification_checked
						&& activated_sentences.at(
								number_of_sentences_preprocessed))) {

			int begin_pos = 0;

			int i = begin_pos * utf8_bytes((unsigned char *) buffer.c_str());

			while (i
					<= buffer.length()
							- charOffset
									* utf8_bytes(
											(unsigned char *) buffer.c_str())) {

				std::string newEntry = buffer.substr(i,
						charOffset
								* utf8_bytes((unsigned char *) buffer.c_str()));

				if (param->verbose_option == 1)
					cerr << "lookup entry: " << newEntry << endl;

				//pre-checking block (unfinished)
				//			lookup_stat = HashFreq.lookup(newEntry, sentences_counter,
				//					i, boundary_estimation_table, param->filter_size);
				//
				//			if (lookup_stat < 0) {
				//				cerr << "terminated unexpectedly in looking up." << endl;
				//				break;
				//			}

				std::vector<Morph::BoundAttrs*> boundAttrsArray =
						HashFreq.lookup_and_load_bound_attrs(newEntry, buffer,
								i, param->filter_size);

				if (!boundAttrsArray.empty()) {

					for (std::vector<Morph::BoundAttrs*>::iterator bound_vec_it =
							boundAttrsArray.begin();
							bound_vec_it != boundAttrsArray.end();
							++bound_vec_it) {

						if (param->config_mode == 6) {
							std::string ms_instance = buffer.substr(i,
									(*bound_vec_it)->end_pos - i);
							quick_ms_list[ms_instance] =
									(*bound_vec_it)->freqEntityRef->freq;
						}

						//read freq tags
						if (HashFreq.freq_category_list.find(
								(*bound_vec_it)->freqEntityRef->freq)
								== HashFreq.freq_category_list.end()) {
							cerr << ";;err: freq category not found" << endl;
						} else {
							(*bound_vec_it)->freq_tag =
									HashFreq.freq_category_list[(*bound_vec_it)->freqEntityRef->freq];

//							cerr << (*bound_vec_it)->freqEntityRef->freq << " "
//									<< (*bound_vec_it)->freq_tag << endl;
						}

						if (new_sentence->beginBoundAttrsTab.find(i)
								== new_sentence->beginBoundAttrsTab.end()) {
							std::vector<Morph::BoundAttrs *> newBAC;
							newBAC.push_back(*bound_vec_it);
							new_sentence->beginBoundAttrsTab[i] = newBAC;

							if (param->verbose_option == 1)
								cerr << "new position for beginBoundAttrs: "
										<< i << endl;

						} else {
							new_sentence->beginBoundAttrsTab[i].push_back(
									*bound_vec_it);

							if (param->verbose_option == 1)
								cerr << "more on position for beginBoundAttrs: "
										<< i << endl;
						}

						if (new_sentence->endBoundAttrsTab.find(
								(*bound_vec_it)->end_pos)
								== new_sentence->endBoundAttrsTab.end()) {
							std::vector<Morph::BoundAttrs *> newBAC;
							newBAC.push_back(*bound_vec_it);
							new_sentence->endBoundAttrsTab[(*bound_vec_it)->end_pos] =
									newBAC;

							if (param->verbose_option == 1)
								cerr << "new position for endBoundAttrs: "
										<< (*bound_vec_it)->end_pos << endl;

						} else {
							new_sentence->endBoundAttrsTab[(*bound_vec_it)->end_pos].push_back(
									*bound_vec_it);

							if (param->verbose_option == 1)
								cerr << "more on position for endBoundAttrs: "
										<< (*bound_vec_it)->end_pos << endl;
						}

					}

				}

				i += utf8_bytes((unsigned char *) (buffer.c_str() + i));
			}

//			cerr << "current sentence to evaluate: " << buffer << endl;

			if (!new_sentence->beginBoundAttrsTab.empty()) {
				AryOfBeginBoundAttrsTab.insert(
						std::make_pair(number_of_sentences_preprocessed,
								new_sentence->beginBoundAttrsTab));
			}

			if (!new_sentence->endBoundAttrsTab.empty()) {
				AryOfEndBoundAttrsTab.insert(
						std::make_pair(number_of_sentences_preprocessed,
								new_sentence->endBoundAttrsTab));
			}

//			cerr << "finished sentence: " << buffer << endl;

		}

		new_sentence->clear_nodes();

		//debug phrase
//		cerr << "pre-test for ms analysis: "
//				<< new_sentence->beginBoundAttrsTab.size() << " ";
//		for (std::map<int, std::vector<Morph::BoundAttrs *> >::iterator bmap_it =
//				new_sentence->beginBoundAttrsTab.begin();
//				bmap_it != new_sentence->beginBoundAttrsTab.end(); ++bmap_it) {
//			if (!bmap_it->second.empty()) {
//				cerr << "gotit ";
//			}
//		}
//		cerr << endl;

		++number_of_sentences_preprocessed;
	}

	if (param->config_mode == 6) {
		print_quick_list();
	}

	//checking block
//	if (param->printing_option == 0)
//		HashFreq.print_structure(param->freq_threshold);
	if (param->printing_option == 1)
		HashFreq.print_from_lookup();
	if (param->printing_option == 2)
		HashFreq.print_freq_sorted(param->freq_threshold);
	if (param->printing_option == 3)
		HashFreq.print_maxsub_alphabet_order(param->freq_threshold);

	cerr << "Preprocessed sentences: " << number_of_sentences_preprocessed
			<< endl;

	cerr << "total sentences in activate check: " << activated_sentences.size()
			<< endl;

	raw_in.close();
	return true;
}

bool Tagger::load_bound_attrs(const std::string &gsd_file,
		const std::string &main_struct_file,
		const std::string &side_struct_file, const std::string &punc_file) {
	return load_bound_attrs(gsd_file.c_str(), main_struct_file.c_str(),
			side_struct_file.c_str(), punc_file.c_str());
}

bool Tagger::load_bound_attrs(const char *gsd_file,
		const char *main_struct_file, const char *side_struct_file,
		const char *punc_file) {

	read_freq_struct_data(side_struct_file, punc_file);

	cascade_freq_struct_data(main_struct_file, punc_file);

	HashFreqReadOnly.open_deck(sentences_for_hash);

	std::ifstream gsd_in(gsd_file, std::ios::in);
	if (!gsd_in.is_open()) {
		cerr << ";; cannot open gold data for reading" << endl;
		return false;
	}

	std::string buffer;
	int charOffset = param->freq_substr_window;

	int sentences_counter = 0;

	while (getline(gsd_in, buffer)) {

		sentences_for_hash.push_back(buffer);

		if (buffer.at(0) == '#') { // comment line (article delimiter)
			++sentences_counter;
			continue;
		} else if (buffer.length()
				<= charOffset * utf8_bytes((unsigned char *) buffer.c_str())) {

			std::vector<std::string> word_pos_pairs;
			split_string(buffer, " ", word_pos_pairs);

			Sentence *new_sentence = new Sentence(strlen(buffer.c_str()),
					&begin_node_list, &end_node_list, &dic, &ftmpl, &subftmpl,
					param);
			for (std::vector<std::string>::iterator it = word_pos_pairs.begin();
					it != word_pos_pairs.end(); it++) {
				new_sentence->lookup_gold_data(*it);
			}
			new_sentence->find_best_path();
			new_sentence->set_feature();
			new_sentence->clear_nodes();
			add_one_sentence_for_train(new_sentence);
			// new_sentence->feature_print();
			++sentences_for_train_num;
			++sentences_counter;
			continue;
		}

		std::vector<std::string> word_pos_pairs;
		split_string(buffer, " ", word_pos_pairs);

		Sentence *new_sentence = new Sentence(strlen(buffer.c_str()),
				&begin_node_list, &end_node_list, &dic, &ftmpl, &subftmpl,
				param);
		for (std::vector<std::string>::iterator it = word_pos_pairs.begin();
				it != word_pos_pairs.end(); it++) {
			new_sentence->lookup_gold_data(*it);
		}
		new_sentence->find_best_path();
		new_sentence->set_feature();
		new_sentence->clear_nodes();

		int lookup_stat = 0;
		int begin_pos = 0;

		int i = begin_pos * utf8_bytes((unsigned char *) buffer.c_str());

		while (i
				<= buffer.length()
						- charOffset
								* utf8_bytes((unsigned char *) buffer.c_str())) {

			std::string newEntry = buffer.substr(i,
					charOffset * utf8_bytes((unsigned char *) buffer.c_str()));

			if (param->verbose_option == 1)
				cerr << "lookup entry: " << newEntry << endl;

			//pre-checking block (unfinished)
//			lookup_stat = HashFreqReadOnly.lookup(newEntry, sentences_counter,
//					i, boundary_estimation_table, param->filter_size);
//
//			if (lookup_stat < 0) {
//				cerr << "terminated unexpectedly in looking up." << endl;
//				break;
//			}

			std::vector<Morph::BoundAttrs*> boundAttrsArray =
					HashFreqReadOnly.lookup_and_load_bound_attrs(newEntry,
							sentences_counter, i, param->filter_size);

			if (!boundAttrsArray.empty()) {

				for (std::vector<Morph::BoundAttrs*>::iterator bound_vec_it =
						boundAttrsArray.begin();
						bound_vec_it != boundAttrsArray.end(); ++bound_vec_it) {

					if (new_sentence->beginBoundAttrsTab.find(i)
							== new_sentence->beginBoundAttrsTab.end()) {
						std::vector<Morph::BoundAttrs *> newBAC;
						newBAC.push_back(*bound_vec_it);
						new_sentence->beginBoundAttrsTab[i] = newBAC;

						if (param->verbose_option == 1)
							cerr << "new position for beginBoundAttrs: " << i
									<< endl;

					} else {
						new_sentence->beginBoundAttrsTab[i].push_back(
								*bound_vec_it);

						if (param->verbose_option == 1)
							cerr << "more on position for beginBoundAttrs: "
									<< i << endl;
					}

					if (new_sentence->endBoundAttrsTab.find(
							(*bound_vec_it)->end_pos)
							== new_sentence->endBoundAttrsTab.end()) {
						std::vector<Morph::BoundAttrs *> newBAC;
						newBAC.push_back(*bound_vec_it);
						new_sentence->endBoundAttrsTab[(*bound_vec_it)->end_pos] =
								newBAC;

						if (param->verbose_option == 1)
							cerr << "new position for endBoundAttrs: "
									<< (*bound_vec_it)->end_pos << endl;

					} else {
						new_sentence->endBoundAttrsTab[(*bound_vec_it)->end_pos].push_back(
								*bound_vec_it);

						if (param->verbose_option == 1)
							cerr << "more on position for endBoundAttrs: "
									<< (*bound_vec_it)->end_pos << endl;
					}

				}

			}

			i += utf8_bytes((unsigned char *) (buffer.c_str() + i));
		}

		add_one_sentence_for_train(new_sentence);
		// new_sentence->feature_print();
		++sentences_for_train_num;
		++sentences_counter;
	}

	cerr << "Sentence count: " << sentences_counter << endl;

	//checking block
//	if (param->config_mode == 4 && param->printing_option == 0)
//		HashFreqReadOnly.print_structure(param->freq_threshold);
	if (param->config_mode == 4 && param->printing_option == 1)
		HashFreqReadOnly.print_from_lookup();
	if (param->config_mode == 4 && param->printing_option == 2)
		HashFreqReadOnly.print_freq_sorted(param->freq_threshold);

	cerr << "content capacity: " << HashFreqReadOnly.cacheFreq.size() << endl;

	gsd_in.close();

	return true;
}

// clear gold standard data
void Tagger::clear_gold_data() {
	for (std::vector<Sentence *>::iterator it = sentences_for_train.begin();
			it != sentences_for_train.end(); it++) {
		delete *it;
	}
	sentences_for_train.clear();
}

// print the best path of a test sentence
void Tagger::print_best_path() {
	sentence->print_best_path();
}

void Tagger::print_N_best_path() {
	sentence->print_N_best_path();
}

void Tagger::print_Juman_style_path() {
	sentence->print_Juman_style_path();
}

bool Tagger::add_one_sentence_for_train(Sentence *in_sentence) {
	sentences_for_train.push_back(in_sentence);
	return true;
}

}
