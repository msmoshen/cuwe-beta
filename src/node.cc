#include "common.h"
#include "node.h"

namespace Morph {

Node::Node() {
	memset(this, 0, sizeof(Node));
}

Node::~Node() {
	if (string_for_print != string)
		delete string_for_print;
	if (end_string != string)
		delete end_string;
	delete string;
	if (feature)
		delete feature;
}

void Node::print() {
	cout << *(string_for_print) << "_" << *pos;
}

void Node::print_juman() {

	cout << id << " ";
	unsigned int last_id = connection.top();
	cout << last_id;

	unsigned int new_id;

	while (!connection.empty()) {
		connection.pop();
		new_id = connection.top();

		if (new_id != last_id) {
			cout << ";" << new_id;
			last_id = new_id;
		}
	}

	cout << " " << starting_pos << " " << starting_pos + length << " "
			<< *(string_for_print) << " " << *(string_for_print) << " "
			<< *(string_for_print) << " " << *(pos) << " " << posid << " "
			<< "N/A" << endl;

}

const char *Node::get_first_char() {
	return string_for_print->c_str();
}

unsigned short Node::get_char_num() {
	if (char_num >= MAX_RESOLVED_CHAR_NUM)
		return MAX_RESOLVED_CHAR_NUM;
	else
		return char_num;
}

}
