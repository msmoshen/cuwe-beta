#ifndef TAGGER_H
#define TAGGER_H

#include "common.h"
#include "dic.h"
#include "sentence.h"
#include "feature.h"
#include "parameter.h"
#include "hashFreq.h"

namespace Morph {

//#define REFLUSH_PERIOD INT_MAX

class Tagger {
	Parameter *param;
	Dic dic;
	FeatureTemplateSet ftmpl;
	FeatureTemplateSet subftmpl;

	bool unknown_word_detection;
	bool shuffle_training_data;

	Sentence *sentence; // current input sentence

	size_t iteration_num;
	int sentences_for_train_num;
	std::vector<Sentence *> sentences_for_train;

	std::vector<Node *> begin_node_list; // position -> list of nodes that begin at this pos
	std::vector<Node *> end_node_list; // position -> list of nodes that end at this pos

	Dic rev_dic;
	std::vector<std::string> sentences_for_hash;
	std::vector<int> activated_sentences;
	int sentences_for_hash_num;
	std::map<std::string, int> punctuation_table;
	std::map<int, std::map<int, std::vector<int> > > boundary_estimation_table;
	std::map<std::string, int> quick_ms_list;

	//	hashFreq *HashFreq;
	//	hashFreq *HashFreqReadOnly;
	hashFreq HashFreq;
	hashFreq HashFreqReadOnly;

	std::map<std::string, double> b_feature_collection_diff_percentage;
	std::map<std::string, int> b_feature_collection_diff;
	std::map<std::string, int> b_good_feature_collection;
	std::map<std::string, int> b_bad_feature_collection;

	std::map<std::string, double> e_feature_collection_diff_percentage;
	std::map<std::string, int> e_feature_collection_diff;
	std::map<std::string, int> e_good_feature_collection;
	std::map<std::string, int> e_bad_feature_collection;

public:

	bool bytes_unification_checked;

	std::map<int, std::map<int, std::vector<Morph::BoundAttrs *> > > AryOfBeginBoundAttrsTab;
	std::map<int, std::map<int, std::vector<Morph::BoundAttrs *> > > AryOfEndBoundAttrsTab;

	Tagger(Parameter *in_param);
	~Tagger();
	Sentence *new_sentence_analyze(std::string &in_sentence);

	Sentence *new_sentence_analyze_transparent(std::string& in_sentence,
			const std::map<int, std::vector<Morph::BoundAttrs *> >* in_refBeginBoundAttrsTab,
			const std::map<int, std::vector<Morph::BoundAttrs *> >* in_refEndBoundAttrsTab);

	Sentence *new_sentence_analyze_transparent(std::string &in_sentence,
			int current_sentence,
			const std::map<int, std::map<int, std::vector<Morph::BoundAttrs *> > >& in_refAryOfBeginBoundAttrsTab,
			const std::map<int, std::map<int, std::vector<Morph::BoundAttrs *> > >& in_refAryOfEndBoundAttrsTab);

	void sentence_clear();
	bool viterbi_at_position(unsigned int pos, Node *r_node);
	void print_best_path();

	void print_N_best_path();
	void print_Juman_style_path();

	bool train();

	bool read_gold_data(const std::string &gsd_file);
	bool read_gold_data(const char *gsd_file);

	bool read_gsd_and_load_bound_attrs(const std::string &gsd_file,
			const std::string &main_struct_file,
			const std::string &side_struct_file, const std::string &punc_file);
	bool read_gsd_and_load_bound_attrs(const char *gsd_file,
			const char *main_struct_file, const char *side_struct_file,
			const char *punc_file);

	bool read_raw_and_load_bound_attrs(const std::string &raw_file,
			const std::string &main_struct_file,
			const std::string &side_struct_file, const std::string &punc_file);
	bool read_raw_and_load_bound_attrs(const char *raw_file,
			const char *main_struct_file, const char *side_struct_file,
			const char *punc_file);

	bool load_bound_attrs(const std::string &gsd_file,
			const std::string &main_struct_file,
			const std::string &side_struct_file, const std::string &punc_file);
	bool load_bound_attrs(const char *gsd_file, const char *main_struct_file,
			const char *side_struct_file, const char *punc_file);

	bool lookup_gold_data(Sentence *sentence, std::string &word_pos_pair);
	bool add_one_sentence_for_train(Sentence *in_sentence);
	void clear_gold_data();

	std::map<int, std::map<int, std::vector<int> > >& get_boundary_estimation_table() {
		return boundary_estimation_table;
	}

	bool make_punc_table(const std::string &punc_file) {
		return make_punc_table(punc_file.c_str());
	}

	bool make_punc_table(const char *punc_file) {
		std::ifstream punc_in(punc_file, std::ios::in);
		if (!punc_in.is_open()) {
			cerr << ";; cannot open punctuation table for reading" << endl;
			return false;
		}

		std::string buffer;
		std::stringstream ss;

		while (getline(punc_in, buffer)) {

			int freq;

			std::vector<std::string> punc_freq_pairs;

			split_string(buffer, "\t", punc_freq_pairs);

			ss << punc_freq_pairs.at(1);
			ss >> freq;
			ss.str("");
			ss.clear();

			punctuation_table[punc_freq_pairs.at(0)] = freq;

		}

		punc_in.close();

		cerr << "punc\tfreq" << endl;
		for (std::map<std::string, int>::iterator map_iter =
				punctuation_table.begin(); map_iter != punctuation_table.end();
				map_iter++) {
			cerr << map_iter->first << "\t" << map_iter->second << endl;
		}
		cerr << "Total: " << punctuation_table.size() << " entries." << endl;

		return true;
	}

	int check_validity(std::string& incoming_sentence) {

		const char* byte_checker = incoming_sentence.c_str();
		unsigned int char_bytes = 0;
		unsigned int i = 0;

		while (i < incoming_sentence.length()) {
			char_bytes = utf8_bytes((unsigned char*) byte_checker);
			if (char_bytes != param->unified_byte_num) {
				std::string probChar(byte_checker, char_bytes);
//				cerr << "problem in char: " << probChar << endl;
				return 0;
			} else {
				std::string validChar(byte_checker, char_bytes);
//				cerr << validChar;
			}
			i += char_bytes;
			byte_checker += char_bytes;
		}

		return 1;
	}

	bool check_bytes_unification(const std::string &gsd_file,
			const std::string &punc_file) {
		return check_bytes_unification(gsd_file.c_str(), punc_file.c_str());
	}

	bool check_bytes_unification(const char *gsd_file, const char *punc_file) {

		make_punc_table(punc_file);

		if (param->verbose_option >= 0)
			cerr << "punc done" << endl;

		std::ifstream gsd_in(gsd_file, std::ios::in);
		if (!gsd_in.is_open()) {
			cerr << ";; cannot open structured frequency data for reading"
					<< endl;
			return false;
		}

		int charOffset = param->freq_substr_window;

//		HashFreqReadOnly = new hashFreq(charOffset, param->decayer_upper_bound,
//				sentences_for_hash, punctuation_table, dic, rev_dic, param);

		HashFreqReadOnly.init(charOffset, sentences_for_hash, punctuation_table,
				dic, rev_dic, param, param->freq_freeze_state, param->decay_gap);

//		std::map<std::string, std::map<std::string, Morph::freqEntity> > cacheFreq;
//		freqEntity newFreqEntity(current_sentence, current_position,
//				round, reached_length);
//		std::map<std::string, Morph::freqEntity> tempMap;
//		it->second[newSeg] = newFreqEntity;

		std::string buffer;
		std::stringstream ss;

		std::string fixed_prefix;

		int sentence_activated_num = 0;

		while (getline(gsd_in, buffer)) {
//			cerr << "incoming: " << buffer << endl;

			if (buffer.at(0) == '#') { //comment line
				continue;
			}

			int active_flag = check_validity(buffer);
			if (active_flag == 1) {
				if (param->printing_option == 4)
					cout << buffer << endl;

				sentence_activated_num++;
			}

			activated_sentences.push_back(active_flag);
		}

		gsd_in.close();

		double active_rate = (double) sentence_activated_num
				/ (double) activated_sentences.size();

		if (param->verbose_option >= 0)
			cerr << "sentences in total: " << activated_sentences.size()
					<< " among which those are activated: "
					<< sentence_activated_num << " rate: " << active_rate
					<< endl;

		return true;

	}

	bool count_freq_segment(const std::string &input_stream,
			const std::string &gsd_file, const std::string &gsd_struct_file,
			const std::string &punc_file) {
		return count_freq_segment(input_stream.c_str(), gsd_file.c_str(),
				gsd_struct_file.c_str(), punc_file.c_str());
	}

	bool count_freq_segment(const char *input_stream, const char *gsd_file,
			const char *gsd_struct_file, const char *punc_file) {

		if (param->verbose_option >= 0)
			cerr << "int limit: " << INT_MAX << endl;

		make_punc_table(punc_file);

		std::ifstream gsd_in(gsd_file, std::ios::in);
		if (!gsd_in.is_open()) {
			cerr << ";; cannot open gold data for reading" << endl;
			return false;
		}

		std::string buffer;
		sentences_for_hash_num = 0;
		int charOffset = param->freq_substr_window;

//		HashFreq = new hashFreq(charOffset, param->decayer_upper_bound,
//				sentences_for_hash, punctuation_table, dic, rev_dic, param);

		HashFreq.init(charOffset, sentences_for_hash, punctuation_table, dic,
				rev_dic, param, param->freq_freeze_state, param->decay_gap);

		if (MODE_STRUCT) {
			read_freq_struct_data(gsd_struct_file, punc_file);
		}

		while (getline(gsd_in, buffer)) {

			sentences_for_hash.push_back(buffer);

			if (param->verbose_option >= 0)
				cerr << "sentence " << sentences_for_hash_num << endl;

			if (buffer.at(0) == '#'
					|| buffer.length()
							< charOffset
									* utf8_bytes(
											(unsigned char *) buffer.c_str())) { // comment line (article delimiter)
				if (param->verbose_option == 1)
					cerr << "passed " << buffer.length() << endl;

				sentences_for_hash_num++;
				continue;
			}

			if (param->verbose_option == 1)
				cerr << "length " << buffer.length() << endl;

			int step_size = 1;

			bool no_punctuation = false;

			int begin_pos = 0;

			while (!no_punctuation) {

				std::string startStr = buffer.substr(
						begin_pos
								* utf8_bytes((unsigned char *) buffer.c_str()),
						charOffset
								* utf8_bytes((unsigned char *) buffer.c_str()));

				if (param->verbose_option == 1)
					cerr << startStr << endl;

				int start_pos_offset = HashFreq.check_first_punctuation(
						startStr);

				if (start_pos_offset == 0) {
					no_punctuation = true;

				} else {
					begin_pos += start_pos_offset;

				}

			}

			int i = begin_pos * utf8_bytes((unsigned char *) buffer.c_str());

			while (i
					<= buffer.length()
							- charOffset
									* utf8_bytes(
											(unsigned char *) buffer.c_str())) {

				if (punctuation_table.find(
						buffer.substr(
								i
										+ (charOffset - 1)
												* utf8_bytes(
														(unsigned char *) (buffer.c_str()
																+ i)),
								utf8_bytes(
										(unsigned char *) (buffer.c_str() + i))))
						!= punctuation_table.end()) {

					if (param->verbose_option == 1)
						cerr << "punc found: "
								<< buffer.substr(
										i
												+ (charOffset - 1)
														* utf8_bytes(
																(unsigned char *) (buffer.c_str()
																		+ i)),
										utf8_bytes(
												(unsigned char *) (buffer.c_str()
														+ i))) << endl;

					step_size = charOffset;

				} else {

					std::string newEntry = buffer.substr(i,
							charOffset
									* utf8_bytes(
											(unsigned char *) buffer.c_str()));

					if (param->verbose_option == 1)
						cerr << "new entry: " << newEntry << endl;

					step_size = HashFreq.entry(newEntry, sentences_for_hash_num,
							i, sentences_for_hash_num);

				}

				if (step_size < 0) {
					cerr << ";;err: terminated unexpectedly." << endl;
					gsd_in.close();
					break;
				}

				if (step_size > 1) {

					no_punctuation = false;

					int next_begin_pos = 0;

					while (!no_punctuation) {

						std::string checkStr =
								buffer.substr(
										i
												+ (step_size + next_begin_pos)
														* utf8_bytes(
																(unsigned char *) buffer.c_str()),
										charOffset
												* utf8_bytes(
														(unsigned char *) buffer.c_str()));

						if (param->verbose_option == 1)
							cerr << checkStr << endl;

						int step_size_offset = HashFreq.check_first_punctuation(
								checkStr);

						if (step_size_offset == 0) {
							no_punctuation = true;
							step_size += next_begin_pos;
						} else {
							next_begin_pos += step_size_offset;
						}

					}
				}

				i += step_size
						* utf8_bytes((unsigned char *) (buffer.c_str() + i));
			}

			if (param->freq_freeze_state > 0
					&& sentences_for_hash_num % param->reflush_period == 0)
				HashFreq.decay_periodical(sentences_for_hash_num);

			sentences_for_hash_num++;
		}

		if (MODE_DATTR)
			HashFreq.record_dic_attrs();

		if (param->verbose_option >= 0)
			cerr << "done." << endl;

//		HashFreq.decay_instant();
//		HashFreq.print_freq(0);

		HashFreq.categorize_freq(param->freq_threshold);

		if (param->config_mode == 1 && param->printing_option == 0) {
			if (MODE_DATTR)
				HashFreq.print_structure_with_dic_attrs(param->freq_threshold);
			else
				HashFreq.print_structure(param->freq_threshold);
		}


		if (param->config_mode == 1 && param->printing_option == 2)
			HashFreq.print_freq_sorted(param->freq_threshold);

		if (param->config_mode == 1 && param->printing_option == 3)
			HashFreq.print_maxsub_alphabet_order(param->freq_threshold);


		if (param->verbose_option >= 0)
			cerr << "content capacity: " << HashFreq.cacheFreq.size() << endl;
//		HashFreq.print(sentences_for_hash);

		if (param->config_mode == 1 && param->printing_option == 1) {
			HashFreq.print_from_lookup();
		}

		gsd_in.close();

		return true;
	}

	int read_freq_data(const std::string &gsd_file) {
		return read_freq_data(gsd_file.c_str());
	}

	int read_freq_data(const char *gsd_file) {

		std::ifstream gsd_in(gsd_file, std::ios::in);
		if (!gsd_in.is_open()) {
			cerr << ";; cannot open structured frequency data for reading"
					<< endl;
			return 0;
		}

		int charOffset = param->freq_substr_window;

		std::vector<std::string> pseudo_sentences;
		std::string buffer;
		std::stringstream ss;

		while (getline(gsd_in, buffer)) {
			if (buffer.at(0) == '#') { // comment line (article delimiter)
				continue;
			}

			std::vector<std::string> string_sequence;
			split_string(buffer, "\t", string_sequence);

//			cerr << string_sequence.at(0) << "\t" << charOffset << "\t" << utf8_bytes(
//					(unsigned char *) string_sequence.at(0).c_str()) << endl;

			if (string_sequence.at(0).length()
					< charOffset
							* utf8_bytes(
									(unsigned char *) string_sequence.at(0).c_str())) {

				cerr << ";;err: max-sub length not expected" << endl;

			} else {

				int newFreq;
				ss << string_sequence.at(1);
				ss >> newFreq;
				ss.str("");
				ss.clear();
				for (int i = 0; i < newFreq; ++i) {
					pseudo_sentences.push_back(string_sequence.at(0));
				}

			}

		}

		sentences_for_hash_num = 0;

		HashFreq.init(charOffset, sentences_for_hash, punctuation_table, dic,
				rev_dic, param, param->freq_freeze_state, param->decay_gap);

		for (std::vector<std::string>::const_iterator ps_it =
				pseudo_sentences.begin(); ps_it != pseudo_sentences.end();
				++ps_it) {

			buffer = *ps_it;

			sentences_for_hash.push_back(buffer);

			if (param->verbose_option >= 0)
				cerr << "sentence " << sentences_for_hash_num << endl;

			if (buffer.at(0) == '#'
					|| buffer.length()
							< charOffset
									* utf8_bytes(
											(unsigned char *) buffer.c_str())) { // comment line (article delimiter)
				if (param->verbose_option == 1)
					cerr << "passed " << buffer.length() << endl;

				sentences_for_hash_num++;
				continue;
			}

			if (param->verbose_option == 1)
				cerr << "length " << buffer.length() << endl;

			int step_size = 1;

			bool no_punctuation = false;

			int begin_pos = 0;

			while (!no_punctuation) {

				std::string startStr = buffer.substr(
						begin_pos
								* utf8_bytes((unsigned char *) buffer.c_str()),
						charOffset
								* utf8_bytes((unsigned char *) buffer.c_str()));

				if (param->verbose_option == 1)
					cerr << startStr << endl;

				int start_pos_offset = HashFreq.check_first_punctuation(
						startStr);

				if (start_pos_offset == 0) {
					no_punctuation = true;

				} else {
					begin_pos += start_pos_offset;

				}

			}

			int i = begin_pos * utf8_bytes((unsigned char *) buffer.c_str());

			while (i
					<= buffer.length()
							- charOffset
									* utf8_bytes(
											(unsigned char *) buffer.c_str())) {

				if (punctuation_table.find(
						buffer.substr(
								i
										+ (charOffset - 1)
												* utf8_bytes(
														(unsigned char *) (buffer.c_str()
																+ i)),
								utf8_bytes(
										(unsigned char *) (buffer.c_str() + i))))
						!= punctuation_table.end()) {

					if (param->verbose_option == 1)
						cerr << "punc found: "
								<< buffer.substr(
										i
												+ (charOffset - 1)
														* utf8_bytes(
																(unsigned char *) (buffer.c_str()
																		+ i)),
										utf8_bytes(
												(unsigned char *) (buffer.c_str()
														+ i))) << endl;

					step_size = charOffset;

				} else {

					std::string newEntry = buffer.substr(i,
							charOffset
									* utf8_bytes(
											(unsigned char *) buffer.c_str()));

					if (param->verbose_option == 1)
						cerr << "new entry: " << newEntry << endl;

					step_size = HashFreq.entry(newEntry, sentences_for_hash_num,
							i, sentences_for_hash_num);

				}

				if (step_size < 0) {
					cerr << ";;err: terminated unexpectedly." << endl;
					gsd_in.close();
					break;
				}

				if (step_size > 1) {

					no_punctuation = false;

					int next_begin_pos = 0;

					while (!no_punctuation) {

						std::string checkStr =
								buffer.substr(
										i
												+ (step_size + next_begin_pos)
														* utf8_bytes(
																(unsigned char *) buffer.c_str()),
										charOffset
												* utf8_bytes(
														(unsigned char *) buffer.c_str()));

						if (param->verbose_option == 1)
							cerr << checkStr << endl;

						int step_size_offset = HashFreq.check_first_punctuation(
								checkStr);

						if (step_size_offset == 0) {
							no_punctuation = true;
							step_size += next_begin_pos;
						} else {
							next_begin_pos += step_size_offset;
						}

					}
				}

				i += step_size
						* utf8_bytes((unsigned char *) (buffer.c_str() + i));
			}

			sentences_for_hash_num++;
		}

		if (param->verbose_option >= 0)
			cerr << "done." << endl;

		gsd_in.close();

		if (param->printing_option == 2)
			HashFreq.print_freq_sorted(param->freq_threshold);
		if (param->printing_option == 3)
			HashFreq.print_maxsub_alphabet_order(param->freq_threshold);

		return sentences_for_train_num;
	}

	bool cascade_freq_data(const std::string &gsd_file, int starting_num) {
		return cascade_freq_data(gsd_file.c_str(), starting_num);
	}

	bool cascade_freq_data(const char *gsd_file, int starting_num) {

		if (!HashFreq.initiated) {
			cerr << ";;err: hashfreqRO not initialized\n" << endl;
			return false;
		}

		std::ifstream gsd_in(gsd_file, std::ios::in);
		if (!gsd_in.is_open()) {
			cerr << ";; cannot open structured frequency data for reading"
					<< endl;
			return false;
		}

		int charOffset = param->freq_substr_window;

		std::vector<std::string> pseudo_sentences;
		std::string buffer;
		std::stringstream ss;

		while (getline(gsd_in, buffer)) {
			if (buffer.at(0) == '#') { // comment line (article delimiter)
				continue;
			}

			std::vector<std::string> string_sequence;
			split_string(buffer, "\t", string_sequence);

//			cerr << string_sequence.at(0) << "\t" << charOffset << "\t" << utf8_bytes(
//					(unsigned char *) string_sequence.at(0).c_str()) << endl;

			if (string_sequence.at(0).length()
					< charOffset
							* utf8_bytes(
									(unsigned char *) string_sequence.at(0).c_str())) {

				cerr << ";;err: max-sub length not expected" << endl;

			} else {

				int newFreq;
				ss << string_sequence.at(1);
				ss >> newFreq;
				ss.str("");
				ss.clear();
				for (int i = 0; i < newFreq; ++i) {
					pseudo_sentences.push_back(string_sequence.at(0));
				}

			}

		}

		buffer.clear();
		sentences_for_hash_num = starting_num;

		for (int i = 0; i < sentences_for_hash_num; ++i) {
			sentences_for_hash.push_back(buffer); //stacking dummy
		}

		for (std::vector<std::string>::const_iterator ps_it =
				pseudo_sentences.begin(); ps_it != pseudo_sentences.end();
				++ps_it) {

			buffer = *ps_it;

			sentences_for_hash.push_back(buffer);

			if (param->verbose_option >= 0)
				cerr << "sentence " << sentences_for_hash_num << endl;

			if (buffer.at(0) == '#'
					|| buffer.length()
							< charOffset
									* utf8_bytes(
											(unsigned char *) buffer.c_str())) { // comment line (article delimiter)
				if (param->verbose_option == 1)
					cerr << "passed " << buffer.length() << endl;

				sentences_for_hash_num++;
				continue;
			}

			if (param->verbose_option == 1)
				cerr << "length " << buffer.length() << endl;

			int step_size = 1;

			bool no_punctuation = false;

			int begin_pos = 0;

			while (!no_punctuation) {

				std::string startStr = buffer.substr(
						begin_pos
								* utf8_bytes((unsigned char *) buffer.c_str()),
						charOffset
								* utf8_bytes((unsigned char *) buffer.c_str()));

				if (param->verbose_option == 1)
					cerr << startStr << endl;

				int start_pos_offset = HashFreq.check_first_punctuation(
						startStr);

				if (start_pos_offset == 0) {
					no_punctuation = true;

				} else {
					begin_pos += start_pos_offset;

				}

			}

			int i = begin_pos * utf8_bytes((unsigned char *) buffer.c_str());

			while (i
					<= buffer.length()
							- charOffset
									* utf8_bytes(
											(unsigned char *) buffer.c_str())) {

				if (punctuation_table.find(
						buffer.substr(
								i
										+ (charOffset - 1)
												* utf8_bytes(
														(unsigned char *) (buffer.c_str()
																+ i)),
								utf8_bytes(
										(unsigned char *) (buffer.c_str() + i))))
						!= punctuation_table.end()) {

					if (param->verbose_option == 1)
						cerr << "punc found: "
								<< buffer.substr(
										i
												+ (charOffset - 1)
														* utf8_bytes(
																(unsigned char *) (buffer.c_str()
																		+ i)),
										utf8_bytes(
												(unsigned char *) (buffer.c_str()
														+ i))) << endl;

					step_size = charOffset;

				} else {

					std::string newEntry = buffer.substr(i,
							charOffset
									* utf8_bytes(
											(unsigned char *) buffer.c_str()));

					if (param->verbose_option == 1)
						cerr << "new entry: " << newEntry << endl;

					step_size = HashFreq.entry(newEntry, sentences_for_hash_num,
							i, sentences_for_hash_num);

				}

				if (step_size < 0) {
					cerr << ";;err: terminated unexpectedly." << endl;
					gsd_in.close();
					break;
				}

				if (step_size > 1) {

					no_punctuation = false;

					int next_begin_pos = 0;

					while (!no_punctuation) {

						std::string checkStr =
								buffer.substr(
										i
												+ (step_size + next_begin_pos)
														* utf8_bytes(
																(unsigned char *) buffer.c_str()),
										charOffset
												* utf8_bytes(
														(unsigned char *) buffer.c_str()));

						if (param->verbose_option == 1)
							cerr << checkStr << endl;

						int step_size_offset = HashFreq.check_first_punctuation(
								checkStr);

						if (step_size_offset == 0) {
							no_punctuation = true;
							step_size += next_begin_pos;
						} else {
							next_begin_pos += step_size_offset;
						}

					}
				}

				i += step_size
						* utf8_bytes((unsigned char *) (buffer.c_str() + i));
			}

			sentences_for_hash_num++;
		}

		if (param->verbose_option >= 0)
			cerr << "done." << endl;

		gsd_in.close();

		if (param->printing_option == 20)
			HashFreq.print_freq_sorted(param->freq_threshold);
		if (param->printing_option == 30)
			HashFreq.print_maxsub_alphabet_order(param->freq_threshold);

		return true;

	}

	bool read_freq_struct_data(const std::string &gsd_file,
			const std::string &punc_file) {
		return read_freq_struct_data(gsd_file.c_str(), punc_file.c_str());
	}

	bool read_freq_struct_data(const char *gsd_file, const char *punc_file) {

		make_punc_table(punc_file);

		if (param->verbose_option >= 0)
			cerr << "punc done" << endl;

		std::ifstream gsd_in(gsd_file, std::ios::in);
		if (!gsd_in.is_open()) {
			cerr << ";; cannot open structured frequency data for reading"
					<< endl;
			return false;
		}

		int charOffset = param->freq_substr_window;

//		HashFreq = new hashFreq(charOffset, param->decayer_upper_bound,
//				sentences_for_hash, punctuation_table, dic, rev_dic, param);

		HashFreq.init_and_close(charOffset, punctuation_table, dic, rev_dic,
				param, param->freq_freeze_state, param->decay_gap);

//		std::map<std::string, std::map<std::string, Morph::freqEntity> > cacheFreq;
//		freqEntity newFreqEntity(current_sentence, current_position,
//				round, reached_length);
//		std::map<std::string, Morph::freqEntity> tempMap;
//		it->second[newSeg] = newFreqEntity;

		std::string buffer;
		std::stringstream ss;

		std::string fixed_prefix;

		while (getline(gsd_in, buffer)) {
			if (buffer.at(0) == '#') { // comment line (article delimiter)
				continue;
			}

			std::string::iterator indent_counter = buffer.begin();
			int counter = 0;
			std::vector<std::string> string_sequence;

			while (*indent_counter == '\t') {
				++counter;
				++indent_counter;
			}

			if (counter == 0) {
				std::string buffer_remainder(indent_counter, buffer.end());
				split_string(buffer_remainder, "\t", string_sequence);

//				cerr << string_sequence[0] << "\t" << string_sequence[1] << "\t"
//						<< string_sequence[2] << "\t" << endl;

				fixed_prefix = string_sequence.at(0);

				std::map<std::string, Morph::freqEntity> tempLv2Map;

				HashFreq.cacheFreq[fixed_prefix] = tempLv2Map;

			} else if (counter == 1) {
				std::string buffer_remainder(indent_counter, buffer.end());
				split_string(buffer_remainder, "\t", string_sequence);

				int newStrLen;
				int newNumOfEntries;

				int in_b_conf = 0;
				int in_e_conf = 0;

				ss << string_sequence.at(1);
				ss >> newStrLen;
				ss.str("");
				ss.clear();

				ss << string_sequence.at(3);
				ss >> newNumOfEntries;
				ss.str("");
				ss.clear();

				if (string_sequence.size() > 6) {
					ss << string_sequence.at(5);
					ss >> in_b_conf;
					ss.str("");
					ss.clear();

					ss << string_sequence.at(6);
					ss >> in_e_conf;
					ss.str("");
					ss.clear();
				}

				freqEntity newFreqEntity(-1, newStrLen, newNumOfEntries);

				newFreqEntity.update_b_conf(in_b_conf);
				newFreqEntity.update_e_conf(in_e_conf);

				(HashFreq.cacheFreq[fixed_prefix])[string_sequence.at(0)] =
						newFreqEntity;

				//load categorized-freq during reading struct file; not such a good idea
//				if ( HashFreq.freq_category_list.find(newNumOfEntries) != HashFreq.freq_category_list.end() ) {
//					if(HashFreq.freq_category_list[newNumOfEntries] != string_sequence.at(4))
//						cerr << ";;err: freq category not match" << endl;
//				}else{
//					HashFreq.freq_category_list[newNumOfEntries] = string_sequence.at(4);
//				}

//				cerr << "\t" << string_sequence[0] << "\t" << newStrLen << "\t"
//						<< string_sequence[2] << "\t" << endl;

			} else if (counter == 2) {

//				std::string buffer_remainder(indent_counter, buffer.end());
//				split_string(buffer_remainder, "\t", string_sequence);
//
//				cerr << "\t\t" << string_sequence[0] << "\t"
//						<< string_sequence[1] << "\t" << endl;

			} else {
				cerr << ";;err: wrongly formatted data" << endl;
			}

		}

		gsd_in.close();

		//just for a checking

//		HashFreq.print_structure(0);

		if (param->config_mode == 2 && param->printing_option == 2)
			HashFreq.print_freq_sorted(param->freq_threshold);

		if (param->config_mode == 2 && param->printing_option == 3)
			HashFreq.print_maxsub_alphabet_order(param->freq_threshold);

		return true;
	}

	bool cascade_freq_struct_data(const std::string &gsd_file,
			const std::string &punc_file) {
		return cascade_freq_struct_data(gsd_file.c_str(), punc_file.c_str());
	}

	bool cascade_freq_struct_data(const char *gsd_file, const char *punc_file) {

		std::ifstream gsd_in(gsd_file, std::ios::in);
		if (!gsd_in.is_open()) {
			cerr << ";; cannot open structured frequency data for reading"
					<< endl;
			return false;
		}

		int charOffset = param->freq_substr_window;

		if (!HashFreq.initiated) {
			cerr << ";;err: hashfreqRO not initialized\n" << endl;
			return false;
		}

		std::string buffer;
		std::stringstream ss;

		std::string fixed_prefix;

		while (getline(gsd_in, buffer)) {
			if (buffer.at(0) == '#') { // comment line (article delimiter)
				continue;
			}

			std::string::iterator indent_counter = buffer.begin();
			int counter = 0;
			std::vector<std::string> string_sequence;

			while (*indent_counter == '\t') {
				++counter;
				++indent_counter;
			}

			if (counter == 0) {
				std::string buffer_remainder(indent_counter, buffer.end());
				split_string(buffer_remainder, "\t", string_sequence);

//				cerr << string_sequence[0] << "\t" << string_sequence[1] << "\t"
//						<< string_sequence[2] << "\t" << endl;

				fixed_prefix = string_sequence.at(0);

				//fix_prefix not exists in struct file
				if (HashFreq.cacheFreq.find(fixed_prefix)
						== HashFreq.cacheFreq.end()) {
					std::map<std::string, Morph::freqEntity> tempLv2Map;
					HashFreq.cacheFreq[fixed_prefix] = tempLv2Map;
				}

			} else if (counter == 1) {
				std::string buffer_remainder(indent_counter, buffer.end());
				split_string(buffer_remainder, "\t", string_sequence);

				int newStrLen;
				int newNumOfEntries;

				int in_b_conf = 0;
				int in_e_conf = 0;

				ss << string_sequence.at(1);
				ss >> newStrLen;
				ss.str("");
				ss.clear();

				ss << string_sequence.at(3);
				ss >> newNumOfEntries;
				ss.str("");
				ss.clear();

				if (string_sequence.size() > 6) {
					ss << string_sequence.at(5);
					ss >> in_b_conf;
					ss.str("");
					ss.clear();

					ss << string_sequence.at(6);
					ss >> in_e_conf;
					ss.str("");
					ss.clear();
				}

				//flex_prefix not exists in struct file
				if ((HashFreq.cacheFreq[fixed_prefix]).find(string_sequence[0])
						== (HashFreq.cacheFreq[fixed_prefix]).end()) {
					freqEntity newFreqEntity(-1, newStrLen, newNumOfEntries);

					newFreqEntity.update_b_conf(in_b_conf);
					newFreqEntity.update_e_conf(in_e_conf);

					(HashFreq.cacheFreq[fixed_prefix])[string_sequence[0]] =
							newFreqEntity;
				} else {
					(HashFreq.cacheFreq[fixed_prefix])[string_sequence[0]].update_freq(
							newNumOfEntries);
					(HashFreq.cacheFreq[fixed_prefix])[string_sequence[0]].update_b_conf(
							in_b_conf);
					(HashFreq.cacheFreq[fixed_prefix])[string_sequence[0]].update_e_conf(
							in_e_conf);
				}

//				cerr << "\t" << string_sequence[0] << "\t" << newStrLen << "\t"
//						<< string_sequence[2] << "\t" << endl;

			} else if (counter == 2) {

//				std::string buffer_remainder(indent_counter, buffer.end());
//				split_string(buffer_remainder, "\t", string_sequence);
//
//				cerr << "\t\t" << string_sequence[0] << "\t"
//						<< string_sequence[1] << "\t" << endl;

			} else {
				cerr << ";;err: wrongly formatted data" << endl;
			}

		}

		gsd_in.close();

		//just for a checking

//		HashFreq.print_structure(0);

		if (param->config_mode == 2) {
			HashFreq.print_freq_sorted(param->freq_threshold);
//			HashFreq.print_freq(param->freq_threshold);
		}

		return true;
	}

	bool convert_gsd_to_raw(const std::string &gsd_file) {
		return convert_gsd_to_raw(gsd_file.c_str());
	}

	bool convert_gsd_to_raw(const char *gsd_file) {

		std::ifstream gsd_in(gsd_file, std::ios::in);
		if (!gsd_in.is_open()) {
			cerr << ";; cannot open gold data for reading" << endl;
			return false;
		}

		std::string buffer;
		while (getline(gsd_in, buffer)) {
			if (buffer.at(0) == '#') { // comment line (article delimiter)
				cout << buffer << endl;
				continue;
			}
			std::vector<std::string> word_pos_pairs;
			split_string(buffer, " ", word_pos_pairs);

			Sentence *new_sentence = new Sentence(strlen(buffer.c_str()),
					&begin_node_list, &end_node_list, &dic, &ftmpl, &subftmpl,
					param);
			for (std::vector<std::string>::iterator it = word_pos_pairs.begin();
					it != word_pos_pairs.end(); it++) {
				new_sentence->lookup_gold_data(*it);
			}

			cout << new_sentence->sentence << endl;

		}

		gsd_in.close();
		return true;
	}

	bool check_feature_in_gsd(const std::string &gsd_file,
			const std::string &main_struct_file,
			const std::string &side_struct_file, const std::string &punc_file) {
		return check_feature_in_gsd(gsd_file.c_str(), main_struct_file.c_str(),
				side_struct_file.c_str(), punc_file.c_str());
	}

	bool check_feature_in_gsd(const char *gsd_file,
			const char *main_struct_file, const char *side_struct_file,
			const char *punc_file) {

		read_freq_struct_data(side_struct_file, punc_file);

		cascade_freq_struct_data(main_struct_file, punc_file);

		HashFreq.categorize_freq(param->freq_threshold);

		HashFreq.open_deck(sentences_for_hash);

		int charOffset = param->freq_substr_window;

		std::ifstream gsd_in(gsd_file, std::ios::in);
		if (!gsd_in.is_open()) {
			cerr << ";; cannot open gold data for reading" << endl;
			return false;
		}

		int feature_count = 0;

		std::string buffer;
		while (getline(gsd_in, buffer)) {
			if (buffer.at(0) == '#') { // comment line (article delimiter)
				continue;
			}
			std::vector<std::string> word_pos_pairs;
			split_string(buffer, " ", word_pos_pairs);

			Sentence *new_sentence = new Sentence(strlen(buffer.c_str()),
					&begin_node_list, &end_node_list, &dic, &ftmpl, &subftmpl,
					param);
			for (std::vector<std::string>::iterator it = word_pos_pairs.begin();
					it != word_pos_pairs.end(); it++) {
				new_sentence->lookup_gold_data(*it);
			}
			new_sentence->find_best_path();
//			new_sentence->set_feature();

			if (new_sentence->sentence.length() != new_sentence->length) {
				cerr << ";;err: length of sentence not consistent" << endl;
			}

			if (new_sentence->sentence.length()
					<= charOffset
							* utf8_bytes(
									(unsigned char *) new_sentence->sentence.c_str())) {

//				new_sentence->set_subfeature();
				new_sentence->clear_nodes();
//				add_one_sentence_for_train(new_sentence);

				sentences_for_train_num++;

				sentences_for_hash.push_back(new_sentence->sentence);
				continue;
			}

			cerr << "new sentence: " << new_sentence->sentence << endl;

			new_sentence->mark_best_path();

			sentences_for_hash.push_back(new_sentence->sentence);

			if (!bytes_unification_checked
					|| (bytes_unification_checked
							&& activated_sentences.at(sentences_for_train_num))) {

				int begin_pos = 0;

				int i =
						begin_pos
								* utf8_bytes(
										(unsigned char *) new_sentence->sentence.c_str());

				while (i
						<= new_sentence->sentence.length()
								- charOffset
										* utf8_bytes(
												(unsigned char *) new_sentence->sentence.c_str())) {

					std::string newEntry =
							new_sentence->sentence.substr(i,
									charOffset
											* utf8_bytes(
													(unsigned char *) new_sentence->sentence.c_str()));

					if (param->verbose_option == 1)
						cerr << "lookup entry: " << newEntry << endl;

					//pre-checking block (unfinished)
					//			lookup_stat = HashFreq.lookup(newEntry, sentences_counter,
					//					i, boundary_estimation_table, param->filter_size);
					//
					//			if (lookup_stat < 0) {
					//				cerr << "terminated unexpectedly in looking up." << endl;
					//				break;
					//			}

					std::vector<Morph::BoundAttrs*> boundAttrsArray =
							HashFreq.lookup_and_load_bound_attrs(newEntry,
									new_sentence->sentence, i,
									param->filter_size);

					if (!boundAttrsArray.empty()) {

						for (std::vector<Morph::BoundAttrs*>::iterator bound_vec_it =
								boundAttrsArray.begin();
								bound_vec_it != boundAttrsArray.end();
								++bound_vec_it) {

							//read freq tags
							if (HashFreq.freq_category_list.find(
									(*bound_vec_it)->freqEntityRef->freq)
									== HashFreq.freq_category_list.end()) {
								cerr << ";;err: freq category not found"
										<< endl;
							} else {
								(*bound_vec_it)->freq_tag =
										HashFreq.freq_category_list[(*bound_vec_it)->freqEntityRef->freq];

								cerr << (*bound_vec_it)->freqEntityRef->freq
										<< " " << (*bound_vec_it)->freq_tag
										<< endl;
							}

							++feature_count;

							Node pseudoNode;

							FeatureSet begin_feature_checker(
									new_sentence->get_feature_sub_template());

							begin_feature_checker.extract_maxsub_begin_unigram_feature(
									&pseudoNode, *bound_vec_it);

							if (new_sentence->bound_index.find(
									(*bound_vec_it)->begin_pos)
									!= new_sentence->bound_index.end()) {

								begin_feature_checker.gather_feature(
										b_good_feature_collection);
								begin_feature_checker.diff_feature(
										b_feature_collection_diff, true);

								(*bound_vec_it)->freqEntityRef->update_b_conf(
										1);

							} else {

								begin_feature_checker.gather_feature(
										b_bad_feature_collection);
								begin_feature_checker.diff_feature(
										b_feature_collection_diff, false);

							}

							FeatureSet end_feature_checker(
									new_sentence->get_feature_sub_template());

							end_feature_checker.extract_maxsub_end_unigram_feature(
									&pseudoNode, *bound_vec_it);

							if (new_sentence->bound_index.find(
									(*bound_vec_it)->end_pos)
									!= new_sentence->bound_index.end()) {

								end_feature_checker.gather_feature(
										e_good_feature_collection);
								end_feature_checker.diff_feature(
										e_feature_collection_diff, true);

								(*bound_vec_it)->freqEntityRef->update_e_conf(
										1);

							} else {

								end_feature_checker.gather_feature(
										e_bad_feature_collection);
								end_feature_checker.diff_feature(
										e_feature_collection_diff, false);

							}

						}

					}

					i += utf8_bytes(
							(unsigned char *) (new_sentence->sentence.c_str()
									+ i));
				}

			}

			new_sentence->clear_nodes();
			sentences_for_train_num++;
			sentences_for_hash.push_back(new_sentence->sentence);
		}

		//checking block
		if (param->printing_option == 0)
			HashFreq.print_structure(0);
		if (param->printing_option == 1)
			HashFreq.print_from_lookup();
		if (param->printing_option == 2)
			HashFreq.print_freq_sorted(param->freq_threshold);
		if (param->printing_option == 3)
			HashFreq.clean_structure(param->freq_threshold,
					param->generic_switch);
		if (param->printing_option == 5)
			print_feature_collection(feature_count, 40);

		cerr << "HashFreq capacity: " << HashFreq.cacheFreq.size() << endl;

		gsd_in.close();
		return true;
	}

	bool clean_struct(const std::string &gsd_file,
			const std::string &main_struct_file,
			const std::string &side_struct_file, const std::string &punc_file) {
		return clean_struct(gsd_file.c_str(), main_struct_file.c_str(),
				side_struct_file.c_str(), punc_file.c_str());
	}

	bool clean_struct(const char *gsd_file, const char *main_struct_file,
			const char *side_struct_file, const char *punc_file) {

		if (param->generic_switch == 0) {

			read_freq_struct_data(side_struct_file, punc_file);
			cascade_freq_struct_data(main_struct_file, punc_file);

		} else if (param->generic_switch == 1) {

			int starting_num = read_freq_data(side_struct_file);
			cascade_freq_data(main_struct_file, starting_num);

		} else {
			cerr << ";;err: switching option not implemented" << endl;
			return false;
		}

		HashFreq.categorize_freq(param->freq_threshold);

		HashFreq.open_deck(sentences_for_hash);

		int charOffset = param->freq_substr_window;

		std::ifstream gsd_in(gsd_file, std::ios::in);
		if (!gsd_in.is_open()) {
			cerr << ";; cannot open gold data for reading" << endl;
			return false;
		}

		std::string buffer;
		while (getline(gsd_in, buffer)) {
			if (buffer.at(0) == '#') { // comment line (article delimiter)
				continue;
			}
			std::vector<std::string> word_pos_pairs;
			split_string(buffer, " ", word_pos_pairs);

			Sentence *new_sentence = new Sentence(strlen(buffer.c_str()),
					&begin_node_list, &end_node_list, &dic, &ftmpl, &subftmpl,
					param);
			for (std::vector<std::string>::iterator it = word_pos_pairs.begin();
					it != word_pos_pairs.end(); it++) {
				new_sentence->lookup_gold_data(*it);
			}
			new_sentence->find_best_path();
//			new_sentence->set_feature();

			if (new_sentence->sentence.length() != new_sentence->length) {
				cerr << ";;err: length of sentence not consistent" << endl;
			}

			if (new_sentence->sentence.length()
					<= charOffset
							* utf8_bytes(
									(unsigned char *) new_sentence->sentence.c_str())) {

//				new_sentence->set_subfeature();
				new_sentence->clear_nodes();
//				add_one_sentence_for_train(new_sentence);

				sentences_for_train_num++;

				sentences_for_hash.push_back(new_sentence->sentence);
				continue;
			}

			cerr << "new sentence: " << new_sentence->sentence << endl;

			new_sentence->mark_best_path();

			sentences_for_hash.push_back(new_sentence->sentence);

			if (!bytes_unification_checked
					|| (bytes_unification_checked
							&& activated_sentences.at(sentences_for_train_num))) {

				int begin_pos = 0;
				int i =
						begin_pos
								* utf8_bytes(
										(unsigned char *) new_sentence->sentence.c_str());

				while (i
						<= new_sentence->sentence.length()
								- charOffset
										* utf8_bytes(
												(unsigned char *) new_sentence->sentence.c_str())) {

					std::string newEntry =
							new_sentence->sentence.substr(i,
									charOffset
											* utf8_bytes(
													(unsigned char *) new_sentence->sentence.c_str()));

					if (param->verbose_option == 1)
						cerr << "lookup entry: " << newEntry << endl;

					//pre-checking block (unfinished)
					//			lookup_stat = HashFreq.lookup(newEntry, sentences_counter,
					//					i, boundary_estimation_table, param->filter_size);
					//
					//			if (lookup_stat < 0) {
					//				cerr << "terminated unexpectedly in looking up." << endl;
					//				break;
					//			}

					std::vector<Morph::BoundAttrs*> boundAttrsArray =
							HashFreq.lookup_and_load_bound_attrs(newEntry,
									new_sentence->sentence, i,
									param->filter_size);

					if (!boundAttrsArray.empty()) {

						for (std::vector<Morph::BoundAttrs*>::iterator bound_vec_it =
								boundAttrsArray.begin();
								bound_vec_it != boundAttrsArray.end();
								++bound_vec_it) {

							//read freq tags
							if (HashFreq.freq_category_list.find(
									(*bound_vec_it)->freqEntityRef->freq)
									== HashFreq.freq_category_list.end()) {
								cerr << ";;err: freq category not found"
										<< endl;
							} else {
								(*bound_vec_it)->freq_tag =
										HashFreq.freq_category_list[(*bound_vec_it)->freqEntityRef->freq];

//								cerr << (*bound_vec_it)->freqEntityRef->freq
//										<< " " << (*bound_vec_it)->freq_tag
//										<< endl;
							}

							Node pseudoNode;
							FeatureSet feature_checker(
									new_sentence->get_feature_sub_template());

							feature_checker.extract_maxsub_begin_unigram_feature(
									&pseudoNode, *bound_vec_it);

							if (new_sentence->bound_index.find(
									(*bound_vec_it)->begin_pos)
									!= new_sentence->bound_index.end()) {

								feature_checker.gather_feature(
										b_good_feature_collection);
								feature_checker.diff_feature(
										b_feature_collection_diff, true);

								(*bound_vec_it)->freqEntityRef->update_b_conf(
										1);

							} else {

								feature_checker.gather_feature(
										b_bad_feature_collection);
								feature_checker.diff_feature(
										b_feature_collection_diff, false);

								if (param->verbose_option == 3)
									cerr << "begin "
											<< (*bound_vec_it)->begin_pos
											<< " end "
											<< (*bound_vec_it)->end_pos
											<< " max-sub "
											<< (*bound_vec_it)->max_substr
											<< endl;

								HashFreq.remove(newEntry,
										(*bound_vec_it)->max_substr);

							}

						}

					}

					i += utf8_bytes(
							(unsigned char *) (new_sentence->sentence.c_str()
									+ i));
				}

			}

			new_sentence->clear_nodes();
			sentences_for_train_num++;
			sentences_for_hash.push_back(new_sentence->sentence);
		}

		if (param->printing_option == 0)
			HashFreq.print_structure(0);
		if (param->printing_option == 1)
			HashFreq.print_from_lookup();
		if (param->printing_option == 2)
			HashFreq.print_freq_sorted(param->freq_threshold);

		cerr << "HashFreq capacity: " << HashFreq.cacheFreq.size() << endl;

		gsd_in.close();
		return true;
	}

	//

	bool find_upper_bound(const std::string &gsd_file,
			const std::string &raw_file, const std::string &main_struct_file,
			const std::string &side_struct_file, const std::string &punc_file) {
		return find_upper_bound(gsd_file.c_str(), raw_file.c_str(),
				main_struct_file.c_str(), side_struct_file.c_str(),
				punc_file.c_str());
	}

	//totally wrong
	bool find_upper_bound(const char *gsd_file, const char *raw_file,
			const char *main_struct_file, const char *side_struct_file,
			const char *punc_file) {

		if (param->generic_switch == 0) {

			read_freq_struct_data(side_struct_file, punc_file);
			cascade_freq_struct_data(main_struct_file, punc_file);

		} else if (param->generic_switch == 1) {

			int starting_num = read_freq_data(side_struct_file);
			cascade_freq_data(main_struct_file, starting_num);

		} else {
			cerr << ";;err: switching option not implemented" << endl;
			return false;
		}

		int number_of_sentences_preprocessed = 0;

		HashFreq.categorize_freq(param->freq_threshold);

		HashFreq.open_deck(sentences_for_hash);

		int charOffset = param->freq_substr_window;

		std::ifstream gsd_in(gsd_file, std::ios::in);
		if (!gsd_in.is_open()) {
			cerr << ";; cannot open gold data for reading" << endl;
			return false;
		}

		std::string buffer;
		while (getline(gsd_in, buffer)) {
			if (buffer.at(0) == '#') { // comment line (article delimiter)
				continue;
			}
			std::vector<std::string> word_pos_pairs;
			split_string(buffer, " ", word_pos_pairs);

			Sentence *new_sentence = new Sentence(strlen(buffer.c_str()),
					&begin_node_list, &end_node_list, &dic, &ftmpl, &subftmpl,
					param);
			for (std::vector<std::string>::iterator it = word_pos_pairs.begin();
					it != word_pos_pairs.end(); it++) {
				new_sentence->lookup_gold_data(*it);
			}
			new_sentence->find_best_path();
//			new_sentence->set_feature();

			if (new_sentence->sentence.length() != new_sentence->length) {
				cerr << ";;err: length of sentence not consistent" << endl;
			}

			if (new_sentence->sentence.length()
					<= charOffset
							* utf8_bytes(
									(unsigned char *) new_sentence->sentence.c_str())) {
				++number_of_sentences_preprocessed;
//				new_sentence->set_subfeature();
				new_sentence->clear_nodes();
//				add_one_sentence_for_train(new_sentence);

				continue;
			}

			cerr << "new sentence: " << new_sentence->sentence << endl;

			new_sentence->mark_best_path();

//			sentences_for_hash.push_back(new_sentence->sentence);

			if (!bytes_unification_checked
					|| (bytes_unification_checked
							&& activated_sentences.at(
									number_of_sentences_preprocessed))) {

				int begin_pos = 0;

				int i =
						begin_pos
								* utf8_bytes(
										(unsigned char *) new_sentence->sentence.c_str());

				while (i
						<= new_sentence->sentence.length()
								- charOffset
										* utf8_bytes(
												(unsigned char *) new_sentence->sentence.c_str())) {

					std::string newEntry =
							new_sentence->sentence.substr(i,
									charOffset
											* utf8_bytes(
													(unsigned char *) new_sentence->sentence.c_str()));

					if (param->verbose_option == 1)
						cerr << "lookup entry: " << newEntry << endl;

					//pre-checking block (unfinished)
					//			lookup_stat = HashFreq.lookup(newEntry, sentences_counter,
					//					i, boundary_estimation_table, param->filter_size);
					//
					//			if (lookup_stat < 0) {
					//				cerr << "terminated unexpectedly in looking up." << endl;
					//				break;
					//			}

					std::vector<Morph::BoundAttrs*> boundAttrsArray =
							HashFreq.lookup_and_load_bound_attrs(newEntry,
									new_sentence->sentence, i,
									param->filter_size);

					if (!boundAttrsArray.empty()) {

						for (std::vector<Morph::BoundAttrs*>::iterator bound_vec_it =
								boundAttrsArray.begin();
								bound_vec_it != boundAttrsArray.end();
								++bound_vec_it) {

							//read freq tags
							if (HashFreq.freq_category_list.find(
									(*bound_vec_it)->freqEntityRef->freq)
									== HashFreq.freq_category_list.end()) {
								cerr << ";;err: freq category not found"
										<< endl;
							} else {
								(*bound_vec_it)->freq_tag =
										HashFreq.freq_category_list[(*bound_vec_it)->freqEntityRef->freq];

								cerr << (*bound_vec_it)->freqEntityRef->freq
										<< " " << (*bound_vec_it)->freq_tag
										<< endl;
							}

							//filter at beginning
							if (new_sentence->bound_index.find(
									(*bound_vec_it)->begin_pos)
									!= new_sentence->bound_index.end()) {

								if (new_sentence->beginBoundAttrsTab.find(i)
										== new_sentence->beginBoundAttrsTab.end()) {
									std::vector<Morph::BoundAttrs *> newBAC;
									newBAC.push_back(*bound_vec_it);
									new_sentence->beginBoundAttrsTab[i] =
											newBAC;

									if (param->verbose_option == 1)
										cerr
												<< "new position for beginBoundAttrs: "
												<< i << endl;

								} else {
									new_sentence->beginBoundAttrsTab[i].push_back(
											*bound_vec_it);

									if (param->verbose_option == 1)
										cerr
												<< "more on position for beginBoundAttrs: "
												<< i << endl;
								}
							} else {
								cerr << "wrong begin maxsub: "
										<< (*bound_vec_it)->max_substr << endl;
							}

							//filter at ending
							if (new_sentence->bound_index.find(
									(*bound_vec_it)->end_pos)
									!= new_sentence->bound_index.end()) {

								if (new_sentence->endBoundAttrsTab.find(
										(*bound_vec_it)->end_pos)
										== new_sentence->endBoundAttrsTab.end()) {
									std::vector<Morph::BoundAttrs *> newBAC;
									newBAC.push_back(*bound_vec_it);
									new_sentence->endBoundAttrsTab[(*bound_vec_it)->end_pos] =
											newBAC;

									if (param->verbose_option == 1)
										cerr
												<< "new position for endBoundAttrs: "
												<< (*bound_vec_it)->end_pos
												<< endl;

								} else {
									new_sentence->endBoundAttrsTab[(*bound_vec_it)->end_pos].push_back(
											*bound_vec_it);

									if (param->verbose_option == 1)
										cerr
												<< "more on position for endBoundAttrs: "
												<< (*bound_vec_it)->end_pos
												<< endl;
								}
							} else {
								cerr << "wrong end maxsub: "
										<< (*bound_vec_it)->max_substr << endl;
							}

						}

					}

					i += utf8_bytes(
							(unsigned char *) (new_sentence->sentence.c_str()
									+ i));
				}

				if (!new_sentence->beginBoundAttrsTab.empty()) {
					AryOfBeginBoundAttrsTab.insert(
							std::make_pair(number_of_sentences_preprocessed,
									new_sentence->beginBoundAttrsTab));
				}

				if (!new_sentence->endBoundAttrsTab.empty()) {
					AryOfEndBoundAttrsTab.insert(
							std::make_pair(number_of_sentences_preprocessed,
									new_sentence->endBoundAttrsTab));
				}

			}

			++number_of_sentences_preprocessed;
			new_sentence->clear_nodes();
//			sentences_for_hash.push_back(new_sentence->sentence);
		}

//		if (param->printing_option == 0)
//			HashFreq.print_structure(0);
//		if (param->printing_option == 1)
//			HashFreq.print_from_lookup();
//		if (param->printing_option == 2)
//			HashFreq.print_freq_sorted(param->freq_threshold);

		cerr << "HashFreq capacity: " << HashFreq.cacheFreq.size() << endl;

		gsd_in.close();
		return true;

	}

	//

	void print_feature_collection(double feature_count, double percentage_threshold) {

		std::map<std::string, double> outstanding_feature;

		if (1) {
			cout << "# Bx Positive" << endl;

			std::map<std::string, int>::iterator bx_finder =
					b_good_feature_collection.find("Bx:1");
			double bx_count = bx_finder->second;
			double bx_p_ratio = (bx_count / feature_count) * 100;
			b_feature_collection_diff_percentage[bx_finder->first] = bx_p_ratio;

			cout << bx_finder->first << "\t" << bx_finder->second << "\t"
					<< bx_p_ratio << "%" << endl;

			cout << "# Positive" << endl;

			for (std::map<std::string, int>::iterator it =
					b_good_feature_collection.begin();
					it != b_good_feature_collection.end(); it++) {

				if (it != bx_finder) {

					double positive_nom = it->second;
					double positive_ratio = (positive_nom / bx_count) * 100;
					b_feature_collection_diff_percentage[it->first] =
							positive_ratio;

					cout << it->first << "\t" << it->second << "\t"
							<< positive_ratio << "%" << endl;
				}

			}

			cout << endl;

			cout << "# Bx Negative" << endl;

			bx_finder = b_bad_feature_collection.find("Bx:1");
			bx_count = bx_finder->second;
			double bx_n_ratio = (bx_count / feature_count) * 100;

			cout << bx_finder->first << "\t" << bx_finder->second << "\t"
					<< bx_n_ratio << "%" << endl;

			cout << "# Negative" << endl;

			for (std::map<std::string, int>::iterator it =
					b_bad_feature_collection.begin();
					it != b_bad_feature_collection.end(); it++) {

				if (it != bx_finder) {
					double negative_nom = it->second;
					double negative_ratio = (negative_nom / bx_count) * 100;

					if (b_feature_collection_diff_percentage.find(it->first)
							== b_feature_collection_diff_percentage.end())
						b_feature_collection_diff_percentage[it->first] =
								negative_ratio;
					else
						b_feature_collection_diff_percentage[it->first] -=
								negative_ratio;

					cout << it->first << "\t" << it->second << "\t"
							<< negative_ratio << "%" << endl;
				}

			}

			cout << endl;
			cout << "# Diff" << endl;
			for (std::map<std::string, double>::iterator it =
					b_feature_collection_diff_percentage.begin();
					it != b_feature_collection_diff_percentage.end(); it++) {

				double p_count;
				double n_count;

				if (b_good_feature_collection.find(it->first)
						!= b_good_feature_collection.end()) {
					p_count = b_good_feature_collection[it->first];
				} else {
					p_count = 0;
				}

				if (b_bad_feature_collection.find(it->first)
						!= b_bad_feature_collection.end()) {
					n_count = b_bad_feature_collection[it->first];
				} else {
					n_count = 0;
				}

				double ratio = (p_count / (p_count + n_count)) * 100
						- bx_p_ratio;

				if (ratio > 0)
					ratio = 100 * ratio / (100 - bx_p_ratio);
				else
					ratio = 100 * ratio / bx_p_ratio;

				cout << it->first << "\t" << it->second << "\t\t" << ratio
						<< endl;

				if( ratio*ratio > percentage_threshold*percentage_threshold)
					outstanding_feature[it->first]=ratio;

			}
		}
		//
		cout << "# Ex Positive" << endl;

		std::map<std::string, int>::iterator ex_finder =
				e_good_feature_collection.find("Ex:1");
		double ex_count = ex_finder->second;
		double ex_p_ratio = (ex_count / feature_count) * 100;
		e_feature_collection_diff_percentage[ex_finder->first] = ex_p_ratio;

		cout << ex_finder->first << "\t" << ex_finder->second << "\t"
				<< ex_p_ratio << "%" << endl;

		cout << "# Positive" << endl;

		for (std::map<std::string, int>::iterator it =
				e_good_feature_collection.begin();
				it != e_good_feature_collection.end(); it++) {

			if (it != ex_finder) {

				double positive_nom = it->second;
				double positive_ratio = (positive_nom / ex_count) * 100;
				e_feature_collection_diff_percentage[it->first] =
						positive_ratio;

				cout << it->first << "\t" << it->second << "\t"
						<< positive_ratio << "%" << endl;
			}

		}

		cout << endl;

		cout << "# Ex Negative" << endl;

		ex_finder = e_bad_feature_collection.find("Ex:1");
		ex_count = ex_finder->second;
		double ex_n_ratio = (ex_count / feature_count) * 100;

		cout << ex_finder->first << "\t" << ex_finder->second << "\t"
				<< ex_n_ratio << "%" << endl;

		cout << "# Negative" << endl;

		for (std::map<std::string, int>::iterator it =
				e_bad_feature_collection.begin();
				it != e_bad_feature_collection.end(); it++) {

			if (it != ex_finder) {
				double negative_nom = it->second;
				double negative_ratio = (negative_nom / ex_count) * 100;

				if (e_feature_collection_diff_percentage.find(it->first)
						== e_feature_collection_diff_percentage.end())
					e_feature_collection_diff_percentage[it->first] =
							negative_ratio;
				else
					e_feature_collection_diff_percentage[it->first] -=
							negative_ratio;

				cout << it->first << "\t" << it->second << "\t"
						<< negative_ratio << "%" << endl;
			}

		}

		cout << endl;
		cout << "# Diff" << endl;
		for (std::map<std::string, double>::iterator it =
				e_feature_collection_diff_percentage.begin();
				it != e_feature_collection_diff_percentage.end(); it++) {

			double p_count;
			double n_count;

			if (e_good_feature_collection.find(it->first)
					!= e_good_feature_collection.end()) {
				p_count = e_good_feature_collection[it->first];
			} else {
				p_count = 0;
			}

			if (e_bad_feature_collection.find(it->first)
					!= e_bad_feature_collection.end()) {
				n_count = e_bad_feature_collection[it->first];
			} else {
				n_count = 0;
			}

			double ratio = (p_count / (p_count + n_count)) * 100 - ex_p_ratio;

			if (ratio > 0)
				ratio = 100 * ratio / (100 - ex_p_ratio);
			else
				ratio = 100 * ratio / ex_p_ratio;

			cout << it->first << "\t" << it->second << "\t\t" << ratio << endl;

			if( ratio*ratio > percentage_threshold*percentage_threshold)
				outstanding_feature[it->first]=ratio;

		}

		cout << endl;
		cout << "# Pickup" << endl;
		for (std::map<std::string, double>::iterator it =
				outstanding_feature.begin();
				it != outstanding_feature.end(); it++) {
			cout << it->first << "\t" << it->second << endl;
		}

	}

	void print_quick_list() {
		for (std::map<std::string, int>::iterator it = quick_ms_list.begin();
				it != quick_ms_list.end(); it++) {

			cout << it->first << "\t" << it->second << endl;

		}
	}




	bool count_freq_segment_with_bound_attrs(const char *input_stream, const char *gsd_file,
			const char *gsd_struct_file, const char *punc_file) {

		if (param->verbose_option >= 0)
			cerr << "int limit: " << INT_MAX << endl;

		make_punc_table(punc_file);

		std::ifstream gsd_in(gsd_file, std::ios::in);
		if (!gsd_in.is_open()) {
			cerr << ";; cannot open gold data for reading" << endl;
			return false;
		}

		std::string buffer;
		sentences_for_hash_num = 0;
		int charOffset = param->freq_substr_window;

//		HashFreq = new hashFreq(charOffset, param->decayer_upper_bound,
//				sentences_for_hash, punctuation_table, dic, rev_dic, param);

		HashFreq.init(charOffset, sentences_for_hash, punctuation_table, dic,
				rev_dic, param, param->freq_freeze_state, param->decay_gap);

		if (MODE_STRUCT) {
			//need to be modified
			read_freq_struct_data(gsd_struct_file, punc_file);
		}

		while (getline(gsd_in, buffer)) {

			sentences_for_hash.push_back(buffer);

			if (param->verbose_option >= 0)
				cerr << "sentence " << sentences_for_hash_num << endl;

			if (buffer.at(0) == '#'
					|| buffer.length()
							< charOffset
									* utf8_bytes(
											(unsigned char *) buffer.c_str())) { // comment line (article delimiter)
				if (param->verbose_option == 1)
					cerr << "passed " << buffer.length() << endl;

				sentences_for_hash_num++;
				continue;
			}

			if (param->verbose_option == 1)
				cerr << "length " << buffer.length() << endl;

			int step_size = 1;

			bool no_punctuation = false;

			int begin_pos = 0;

			while (!no_punctuation) {

				std::string startStr = buffer.substr(
						begin_pos
								* utf8_bytes((unsigned char *) buffer.c_str()),
						charOffset
								* utf8_bytes((unsigned char *) buffer.c_str()));

				if (param->verbose_option == 1)
					cerr << startStr << endl;

				int start_pos_offset = HashFreq.check_first_punctuation(
						startStr);

				if (start_pos_offset == 0) {
					no_punctuation = true;

				} else {
					begin_pos += start_pos_offset;

				}

			}

			int i = begin_pos * utf8_bytes((unsigned char *) buffer.c_str());

			while (i
					<= buffer.length()
							- charOffset
									* utf8_bytes(
											(unsigned char *) buffer.c_str())) {

				if (punctuation_table.find(
						buffer.substr(
								i
										+ (charOffset - 1)
												* utf8_bytes(
														(unsigned char *) (buffer.c_str()
																+ i)),
								utf8_bytes(
										(unsigned char *) (buffer.c_str() + i))))
						!= punctuation_table.end()) {

					if (param->verbose_option == 1)
						cerr << "punc found: "
								<< buffer.substr(
										i
												+ (charOffset - 1)
														* utf8_bytes(
																(unsigned char *) (buffer.c_str()
																		+ i)),
										utf8_bytes(
												(unsigned char *) (buffer.c_str()
														+ i))) << endl;

					step_size = charOffset;

				} else {

					std::string newEntry = buffer.substr(i,
							charOffset
									* utf8_bytes(
											(unsigned char *) buffer.c_str()));

					if (param->verbose_option == 1)
						cerr << "new entry: " << newEntry << endl;

					step_size = HashFreq.entry(newEntry, sentences_for_hash_num,
							i, sentences_for_hash_num);

				}

				if (step_size < 0) {
					cerr << ";;err: terminated unexpectedly." << endl;
					gsd_in.close();
					break;
				}

				if (step_size > 1) {

					no_punctuation = false;

					int next_begin_pos = 0;

					while (!no_punctuation) {

						std::string checkStr =
								buffer.substr(
										i
												+ (step_size + next_begin_pos)
														* utf8_bytes(
																(unsigned char *) buffer.c_str()),
										charOffset
												* utf8_bytes(
														(unsigned char *) buffer.c_str()));

						if (param->verbose_option == 1)
							cerr << checkStr << endl;

						int step_size_offset = HashFreq.check_first_punctuation(
								checkStr);

						if (step_size_offset == 0) {
							no_punctuation = true;
							step_size += next_begin_pos;
						} else {
							next_begin_pos += step_size_offset;
						}

					}
				}

				i += step_size
						* utf8_bytes((unsigned char *) (buffer.c_str() + i));
			}

			if (param->freq_freeze_state > 0
					&& sentences_for_hash_num % param->reflush_period == 0)
				HashFreq.decay_periodical(sentences_for_hash_num);

			sentences_for_hash_num++;
		}

		if (param->verbose_option >= 0)
			cerr << "done." << endl;

//		HashFreq.decay_instant();
//		HashFreq.print_freq(0);

		HashFreq.categorize_freq(param->freq_threshold);

		if (param->config_mode == 1 && param->printing_option == 0)
			HashFreq.print_structure(param->freq_threshold);

		if (param->config_mode == 1 && param->printing_option == 2)
			HashFreq.print_freq_sorted(param->freq_threshold);

		if (param->config_mode == 1 && param->printing_option == 3)
			HashFreq.print_maxsub_alphabet_order(param->freq_threshold);


		if (param->verbose_option >= 0)
			cerr << "content capacity: " << HashFreq.cacheFreq.size() << endl;
//		HashFreq.print(sentences_for_hash);

		if (param->config_mode == 1 && param->printing_option == 1) {
			HashFreq.print_from_lookup();
		}

		gsd_in.close();

		return true;
	}

};

}

#endif
