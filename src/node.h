#ifndef NODE_H
#define NODE_H

#include "common.h"
#include "feature.h"

namespace Morph {
class FeatureSet;
class NbestSearchToken;

struct morph_token_t {
	unsigned short lcAttr;
	unsigned short rcAttr;
	unsigned short posid; // id of part of speech
	short wcost; // cost of this morpheme
	// unsigned int feature;
	// unsigned int compound;  /* reserved for noun compound */
};
typedef struct morph_token_t Token;

class Node {
public:
	Node *prev; // best previous node determined by Viterbi algorithm
	Node *next;
	Node *enext; // next node that ends at this position
	Node *bnext; // next node that begins at this position
	// struct morph_path_t  *rpath;
	// struct morph_path_t  *lpath;
	// struct morph_node_t **begin_node_list;
	// struct morph_node_t **end_node_list;
	const char *surface;
	std::string *string;
	std::string *string_for_print;
	std::string *end_string;
	FeatureSet *feature;
	FeatureSet *subFeature;

	// const char *feature;
	unsigned short length; //length of morph
	unsigned short char_num;
	unsigned short rcAttr;
	unsigned short lcAttr;
	unsigned short posid;
	std::string *pos;
	unsigned int char_type;
	unsigned int char_family;
	unsigned int end_char_family;
	unsigned char stat;
	short wcost; // cost of this morpheme
	long cost; // total cost to this node
	struct morph_token_t *token;

	double wcost_ms; // (max-sub) cost of this morpheme
	double cost_ms; // (max-sub) total cost to this node

	//for N-best and Juman-style output
	unsigned int id;
	unsigned int starting_pos; // starting position
	std::priority_queue<unsigned int, std::vector<unsigned int>,
			std::greater<unsigned int> > connection; // id of previous nodes connected
	std::vector<NbestSearchToken> traceList; // keep track of n-best paths

	Node();
	~Node();
	void print();
	void print_juman();
	const char *get_first_char();
	unsigned short get_char_num();
};

class NbestSearchToken {

public:
	long score;
	Node* prevNode; //access to previous trace-list
	int rank; //specify an element in previous trace-list

	NbestSearchToken(Node* pN) {
		prevNode = pN;
	}
	;

	NbestSearchToken(long s, int r) {
		score = s;
		rank = r;
	}
	;

	NbestSearchToken(long s, int r, Node* pN) {
		score = s;
		rank = r;
		prevNode = pN;
	}
	;

	~NbestSearchToken() {

	}
	;

	bool operator<(const NbestSearchToken &right) const {
		if ((*this).score < right.score)
			return true;
		else
			return false;
	}

};

class JumanOutToken {

public:
	Node* node;

	JumanOutToken(Node* nn) {
		node = nn;
	}
	;

	~JumanOutToken() {

	}
	;

	bool operator<(const JumanOutToken &right) const {
		if ((*this).node->id >= right.node->id) {
			if ((*this).node->id == right.node->id) {
				if ((*this).node->length > right.node->length) {
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

};

}

#endif
