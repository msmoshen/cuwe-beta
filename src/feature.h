#ifndef FEATURE_H
#define FEATURE_H

#include "common.h"
#include "node.h"

namespace Morph {

#define FEATURE_MACRO_STRING_WORD "%w"
#define FEATURE_MACRO_WORD 1
#define FEATURE_MACRO_STRING_POS "%p"
#define FEATURE_MACRO_POS 2
#define FEATURE_MACRO_STRING_LENGTH "%l"
#define FEATURE_MACRO_LENGTH 3
#define FEATURE_MACRO_STRING_BEGINNING_CHAR "%bc"
#define FEATURE_MACRO_BEGINNING_CHAR 4
#define FEATURE_MACRO_STRING_ENDING_CHAR "%ec"
#define FEATURE_MACRO_ENDING_CHAR 5
#define FEATURE_MACRO_STRING_BEGINNING_CHAR_TYPE "%bt"
#define FEATURE_MACRO_BEGINNING_CHAR_TYPE 6
#define FEATURE_MACRO_STRING_ENDING_CHAR_TYPE "%et"
#define FEATURE_MACRO_ENDING_CHAR_TYPE 7
#define FEATURE_MACRO_STRING_FEATURE1 "%f1"
#define FEATURE_MACRO_FEATURE1 8

#define FEATURE_MACRO_STRING_LEFT_WORD "%Lw"
#define FEATURE_MACRO_LEFT_WORD 101
#define FEATURE_MACRO_STRING_LEFT_POS "%Lp"
#define FEATURE_MACRO_LEFT_POS 102
#define FEATURE_MACRO_STRING_LEFT_LENGTH "%Ll"
#define FEATURE_MACRO_LEFT_LENGTH 103
#define FEATURE_MACRO_STRING_LEFT_BEGINNING_CHAR "%Lbc"
#define FEATURE_MACRO_LEFT_BEGINNING_CHAR 104
#define FEATURE_MACRO_STRING_LEFT_ENDING_CHAR "%Lec"
#define FEATURE_MACRO_LEFT_ENDING_CHAR 105
#define FEATURE_MACRO_STRING_LEFT_BEGINNING_CHAR_TYPE "%Lbt"
#define FEATURE_MACRO_LEFT_BEGINNING_CHAR_TYPE 106
#define FEATURE_MACRO_STRING_LEFT_ENDING_CHAR_TYPE "%Let"
#define FEATURE_MACRO_LEFT_ENDING_CHAR_TYPE 107

#define FEATURE_MACRO_STRING_RIGHT_WORD "%Rw"
#define FEATURE_MACRO_RIGHT_WORD 201
#define FEATURE_MACRO_STRING_RIGHT_POS "%Rp"
#define FEATURE_MACRO_RIGHT_POS 202
#define FEATURE_MACRO_STRING_RIGHT_LENGTH "%Rl"
#define FEATURE_MACRO_RIGHT_LENGTH 203
#define FEATURE_MACRO_STRING_RIGHT_BEGINNING_CHAR "%Rbc"
#define FEATURE_MACRO_RIGHT_BEGINNING_CHAR 204
#define FEATURE_MACRO_STRING_RIGHT_ENDING_CHAR "%Rec"
#define FEATURE_MACRO_RIGHT_ENDING_CHAR 205
#define FEATURE_MACRO_STRING_RIGHT_BEGINNING_CHAR_TYPE "%Rbt"
#define FEATURE_MACRO_RIGHT_BEGINNING_CHAR_TYPE 206
#define FEATURE_MACRO_STRING_RIGHT_ENDING_CHAR_TYPE "%Ret"
#define FEATURE_MACRO_RIGHT_ENDING_CHAR_TYPE 207

//max-sub macros:general
#define FEATURE_MACRO_STRING_HIT_BEGIN "%b0"
#define FEATURE_MACRO_HIT_BEGIN 801
#define FEATURE_MACRO_STRING_HIT_END "%e0"
#define FEATURE_MACRO_HIT_END 802
#define FEATURE_MACRO_STRING_MS_FREQ "%f0"
#define FEATURE_MACRO_MS_FREQ 803
#define FEATURE_MACRO_STRING_MS_LENGTH "%l0"
#define FEATURE_MACRO_MS_LENGTH 804
#define FEATURE_MACRO_STRING_MS_GREATER "%g0"
#define FEATURE_MACRO_MS_GREATER 805
//#define FEATURE_MACRO_STRING_MS_AORB "%Ab"
//#define FEATURE_MACRO_MS_AORB 806
#define FEATURE_MACRO_STRING_BOUND_JUSTIFY "%bj"
#define FEATURE_MACRO_BOUND_JUSTIFY 807

#define FEATURE_MACRO_STRING_BOUND_MULTIJUSTIFY "%bmj"
#define FEATURE_MACRO_BOUND_MULTIJUSTIFY 808

//#define FEATURE_MACRO_STRING_PERFECT_MATCH "%pf"
//#define FEATURE_MACRO_PERFECT_MATCH 808
#define FEATURE_MACRO_STRING_CROSS_OVER "%co"
#define FEATURE_MACRO_CROSS_OVER 809
#define FEATURE_MACRO_STRING_MS_INTERNAL "%it"
#define FEATURE_MACRO_MS_INTERNAL 810
#define FEATURE_MACRO_STRING_MS_INNER_CONNECTIVITY "%q1"
#define FEATURE_MACRO_MS_INNER_CONNECTIVITY 811
#define FEATURE_MACRO_STRING_MS_CROSS_CONNECTIVITY "%q2"
#define FEATURE_MACRO_MS_CROSS_CONNECTIVITY 812

#define FEATURE_MACRO_STRING_MS_COMPETETION_CROSS "%c1"
#define FEATURE_MACRO_MS_COMPETETION_CROSS 813
#define FEATURE_MACRO_STRING_MS_COMPETETION_MULTICROSS "%c2"
#define FEATURE_MACRO_MS_COMPETETION_MULTICROSS 814


//max-sub macros:bound attributes
#define FEATURE_MACRO_STRING_A1 "%a1"   //B1
#define FEATURE_MACRO_A1 901
#define FEATURE_MACRO_STRING_A2 "%a2"   //B2
#define FEATURE_MACRO_A2 902
#define FEATURE_MACRO_STRING_A3 "%a3"   //B3
#define FEATURE_MACRO_A3 903
#define FEATURE_MACRO_STRING_A4 "%a4"   //B5
#define FEATURE_MACRO_A4 904
#define FEATURE_MACRO_STRING_A5 "%a5"   //C3
#define FEATURE_MACRO_A5 905
#define FEATURE_MACRO_STRING_A6 "%a6"   //B4
#define FEATURE_MACRO_A6 906
#define FEATURE_MACRO_STRING_A7 "%a7"   //C2
#define FEATURE_MACRO_A7 907
#define FEATURE_MACRO_STRING_A8 "%a8"   //E1
#define FEATURE_MACRO_A8 908
#define FEATURE_MACRO_STRING_A9 "%a9"   //C1
#define FEATURE_MACRO_A9 909
#define FEATURE_MACRO_STRING_A10 "%a10" //E2
#define FEATURE_MACRO_A10 910
#define FEATURE_MACRO_STRING_A11 "%a11" //E3
#define FEATURE_MACRO_A11 911
#define FEATURE_MACRO_STRING_A12 "%a12" //E4
#define FEATURE_MACRO_A12 912
#define FEATURE_MACRO_STRING_A13 "%a13" //E5
#define FEATURE_MACRO_A13 913
#define FEATURE_MACRO_STRING_A14 "%a14" //O1
#define FEATURE_MACRO_A14 914
#define FEATURE_MACRO_STRING_A15 "%a15" //O2
#define FEATURE_MACRO_A15 915

class BoundAttrs;

class FeatureTemplate {
    bool is_unigram;
    std::string name;
    std::vector<unsigned int> features;
  public:
    FeatureTemplate(std::string &in_name, std::string &feature_string, bool in_is_unigram);
    bool get_is_unigram() {
        return is_unigram;
    }
    unsigned int interpret_macro(std::string &macro);
    std::string &get_name() {
        return name;
    }
    std::vector<unsigned int> *get_features() {
        return &features;
    }
};

class FeatureTemplateSet {
    std::vector<FeatureTemplate *> templates;
  public:
    bool open(const std::string &template_filename);
    FeatureTemplate *interpret_template(std::string &template_string, bool is_unigram);
    std::vector<FeatureTemplate *> *get_templates() {
        return &templates;
    }
};

class FeatureSet {
    FeatureTemplateSet *ftmpl;
  public:
    std::vector<std::string> fset;
    FeatureSet(FeatureTemplateSet *in_ftmpl);
    ~FeatureSet();
    int calc_inner_product_with_weight();

    void extract_maxsub_begin_unigram_feature(Node* node, Morph::BoundAttrs* boundAttrs);
    void extract_maxsub_end_unigram_feature(Node* node, Morph::BoundAttrs* boundAttrs);

    void extract_unigram_feature(Node *node);
    void extract_bigram_feature(Node *l_node, Node *r_node);
    bool append_feature(FeatureSet *in);
    void minus_feature_from_weight(std::map<std::string, double> &in_feature_weight);
    void plus_feature_from_weight(std::map<std::string, double> &in_feature_weight);
    void minus_feature_from_weight(std::map<std::string, double> &in_feature_weight, int factor);
    void plus_feature_from_weight(std::map<std::string, double> &in_feature_weight, int factor);

    void minus_feature_from_weight(std::map<std::string, double> &in_feature_weight, std::map<std::string, int> &in_feature_count);
    void plus_feature_from_weight(std::map<std::string, double> &in_feature_weight, std::map<std::string, int> &in_feature_count);
    void minus_feature_from_weight(std::map<std::string, double> &in_feature_weight, std::map<std::string, int> &in_feature_count, int factor);
    void plus_feature_from_weight(std::map<std::string, double> &in_feature_weight, std::map<std::string, int> &in_feature_count, int factor);

    void gather_feature(std::map<std::string, int>& f_collection);
    void diff_feature(std::map<std::string, int>& f_collection, bool is_good);

    bool print();
    bool print_err();

};

}

#endif
